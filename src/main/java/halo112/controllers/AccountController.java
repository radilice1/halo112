package halo112.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import halo112.PrivilegeLevel;
import halo112.data.AccountDTO;
import halo112.model.Account;
import halo112.model.Session;

@RestController
public class AccountController {

	@PostMapping(value = "/account/submitRegistration")
	void submitRegistration(@Valid @RequestBody AccountDTO registration) {
		Account account = new Account();
		account.setUsername(registration.getUsername());
		account.setPassword(registration.getPassword());
		account.setAccountType(registration.getAccountType());
		account.setFirstName(registration.getFirstName());
		account.setLastName(registration.getLastName());
		account.setPhone(registration.getPhone());
		account.setEmail(registration.getEmail());
		account.setProfilePicture(registration.getProfilePicture());
		account.setApproved(false);
		account.create();
	}

	@PostMapping(value = "/account/login")
	Integer login(String username, String password) {
		Account account = Account.load(username, password);
		if (!account.getApproved())
			throw new RuntimeException("not registered");
		return Session.create(account.getId());
	}

	@PostMapping(value = "/account/logout")
	void logout(Integer sessionId) {
		Session.delete(sessionId);
	}

	@PostMapping(value = "/account/approveRegistration")
	void approveRegistration(Integer sessionId, Integer accountId) {
		Session.authenticate(sessionId, PrivilegeLevel.ADMINISTRATOR);
		Account account = Account.load(accountId);
		account.setApproved(true);
		account.update();
	}

	@PostMapping(value = "/account/delete")
	void delete(Integer sessionId, Integer accountId) {
		Session.authenticate(sessionId, PrivilegeLevel.ADMINISTRATOR);
		Account.delete(accountId);
	}

	@PostMapping(value = "/account/update")
	void update(Integer sessionId, @Valid @RequestBody AccountDTO account) {
		Session.authenticateAccount(sessionId, account.getId());
		Account updateAccount = Account.load(account.getId());

		if (account.getAccountType() != null)
			updateAccount.setAccountType(account.getAccountType());
		if (account.getUsername() != null)
			updateAccount.setUsername(account.getUsername());
		if (account.getPassword() != null)
			updateAccount.setPassword(account.getPassword());
		if (account.getFirstName() != null)
			updateAccount.setFirstName(account.getFirstName());
		if (account.getLastName() != null)
			updateAccount.setLastName(account.getLastName());
		if (account.getPhone() != null)
			updateAccount.setPhone(account.getPhone());
		if (account.getEmail() != null)
			updateAccount.setEmail(account.getEmail());
		if (account.getProfilePicture() != null)
			updateAccount.setProfilePicture(account.getProfilePicture());

		updateAccount.update();
	}

	@PostMapping(value = "/account/getForSession")
	AccountDTO getForSession(Integer sessionId) {
		Session session = Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Account account = Account.load(session.getAccountId());
		return new AccountDTO(account);
	}

	@GetMapping(value = "/account/list")
	AccountDTO[] list(Integer sessionId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);

		List<Account> accounts = Account.loadAll();
		AccountDTO[] dtos = new AccountDTO[accounts.size()];
		for (int i = 0; i < dtos.length; i++) {
			dtos[i] = new AccountDTO(accounts.get(i));
		}

		return dtos;
	}

}

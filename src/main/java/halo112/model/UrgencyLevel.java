package halo112.model;

public enum UrgencyLevel {
	LOW,
	MEDIUM,
	HIGH,
	VERY_HIGH
}

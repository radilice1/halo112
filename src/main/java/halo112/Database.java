package halo112;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Database {
	private static Connection connection;

	public static synchronized Connection getConnection() {
		if (connection == null) {
			try {
				Class.forName("org.postgresql.Driver");

				String address = "jdbc:postgresql://ec2-44-194-116-221.compute-1.amazonaws.com:5432/dd7e0grsanlo06";
				String username = "bljsycpnxrowxz";
				String password = "aa4ebab8cdaef9b6e6f2e4be0cc1970106c8a20b03f7b1a5d841731a995f9bae";
				connection = DriverManager.getConnection(address, username, password);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return connection;
	}

	public static void execute(String sql) {
		query(sql, null, null, null);
	}

	public static void execute(String sql, ParameterPopulator populator) {
		query(sql, populator, null, null);
	}

	public static <T> List<T> update(String sql, RowCallback<T> keyCallback) {
		return query(sql, null, null, keyCallback);
	}

	public static <T> List<T> update(String sql, ParameterPopulator populator, RowCallback<T> keyCallback) {
		return query(sql, populator, null, keyCallback);
	}

	public static <T> List<T> query(String sql, RowCallback<T> rowCallback) {
		return query(sql, null, rowCallback, null);
	}

	public static <T> List<T> query(String sql, ParameterPopulator populator, RowCallback<T> rowCallback) {
		return query(sql, populator, rowCallback, null);
	}

	private static <T> List<T> query(String sql, ParameterPopulator populator, RowCallback<T> rowCallback,
			RowCallback<T> keyCallback) {
		ArrayList<T> result = new ArrayList<T>();
		try (PreparedStatement statement = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			if (populator != null)
				populator.populate(statement);
			
			boolean hasResults = statement.execute();

			if (keyCallback != null) {
				try (ResultSet set = statement.getGeneratedKeys()) {
					while (set.next())
						result.add(keyCallback.row(set));
				}
			}
			
			while (hasResults) {
				try (ResultSet set = statement.getResultSet()) {
					if (rowCallback != null) {
						while (set.next())
							result.add(rowCallback.row(set));
					}
				}
				
				hasResults = statement.getMoreResults();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	public static interface ParameterPopulator {
		public void populate(PreparedStatement statement) throws SQLException;
	}

	public static interface RowCallback<T> {
		public T row(ResultSet set) throws SQLException;
	}
}

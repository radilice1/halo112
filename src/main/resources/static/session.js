import { Router } from "./router.js";

export let Session = {

    id: null,
    account: null,

    removeFromStorage: function(urlToFollowIfBad) {
        Session.account = null;
        Session.id = null;
        localStorage.removeItem("session");
        Router.follow(urlToFollowIfBad || "#login");
    },

    checkLoggedIn: function() {
        if (!Session.id) {
            Router.replaceContent(Router.bodyContent, Router.template("default"));
            Session.checkSessionInStorage(window.location.pathname + window.location.search + window.location.hash);
            return false;
        }
        return true;
    },

    checkSessionInStorage: function(urlToFollowIfOk) {
        let session = localStorage.getItem("session");

        fetch("/account/getForSession?" + new URLSearchParams({ sessionId: session }), { method: "post" })
        .then(async (response) => {
            if (response.status == 200) {
                Session.account = await response.json();
                Session.id = session;
                if (urlToFollowIfOk)
                    Router.follow(urlToFollowIfOk)
            } else {
                Session.removeFromStorage();
            }
        })
        .catch((error) => {
            console.error(error);
            Session.removeFromStorage();
        })
    },

    login: function(urlToFollowIfOk, urlToFollowIfBad, username, password) {
        fetch("/account/login?" + new URLSearchParams({ username: username, password: password }), { method: "post" })
        .then(async (response) => {
            if (response.status == 200) {
                localStorage.setItem("session", await response.json());
                Session.checkSessionInStorage(urlToFollowIfOk);
            } else {
                Session.removeFromStorage(urlToFollowIfBad);
            }
        })
        .catch((error) => {
            console.error(error);
            Session.removeFromStorage(urlToFollowIfBad);
        })
    },

    logout: function() {
        fetch("/account/logout?" + new URLSearchParams({ sessionId: Session.id }), { method: "post" });
        Session.removeFromStorage();
    },

    register: function(urlToFollowIfOk, urlToFollowIfBad, dto) {
        fetch("/account/submitRegistration", { method: "post", headers: { "Content-Type": "application/json" }, body: JSON.stringify(dto) })
        .then(async (response) => {
            if (response.status == 200) {
                Router.follow(urlToFollowIfOk);
            } else {
                Session.removeFromStorage(urlToFollowIfBad);
            }
        })
        .catch((error) => {
            console.error(error);
            Session.removeFromStorage(urlToFollowIfBad);
        })
    }

};

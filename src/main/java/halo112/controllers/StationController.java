package halo112.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import halo112.PrivilegeLevel;
import halo112.data.RescuerDTO;
import halo112.data.StationDTO;
import halo112.model.Rescuer;
import halo112.model.Session;
import halo112.model.Station;

@RestController
public class StationController {
	@GetMapping(value = "/station/list")
	StationDTO[] list(Integer sessionId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		List<Station> stations = Station.loadAll();
		StationDTO[] result = new StationDTO[stations.size()];
		for (int i = 0; i < result.length; i++)
		{
			Station station = stations.get(i);
			StationDTO dto = new StationDTO();
			dto.setId(station.getId());
			dto.setName(station.getName());
			dto.setOwnerId(station.getOwnerId());
			result[i] = dto;
		}
		return result;
	}
	
	@PostMapping(value = "/station/create")
	void createStation(Integer sessionId, @Valid @RequestBody StationDTO stationDTO) {
		Session.authenticate(sessionId, PrivilegeLevel.ADMINISTRATOR);
		Station station = new Station();
		station.setName(stationDTO.getName());
		station.setOwnerId(stationDTO.getOwnerId());
		station.store();
	}
	
	@PostMapping(value = "/station/update")
	void updateStation(Integer sessionId, @Valid @RequestBody StationDTO stationDTO) {
		Session.authenticate(sessionId, PrivilegeLevel.ADMINISTRATOR);
		Station station = Station.load(stationDTO.getId());
		station.setName(stationDTO.getName());
		station.setOwnerId(stationDTO.getOwnerId());
		station.update();

		if (stationDTO.getOwnerId() != null) {
			Rescuer rescuer = Rescuer.load(stationDTO.getOwnerId());
			rescuer.setStationId(station.getId());
			rescuer.update();
		}
	}

	@PostMapping(value = "/station/delete")
	void deleteStation(Integer sessionId, Integer stationId) {
		Session.authenticate(sessionId, PrivilegeLevel.ADMINISTRATOR);
		Station.delete(stationId);
	}

	@PostMapping(value = "/station/addMember")
	void addMember(Integer sessionId, Integer stationId, Integer accountId) {
		Session.authenticateAccount(sessionId, Station.load(stationId).getOwnerId());
		Rescuer rescuer = Rescuer.load(accountId);
		rescuer.setStationId(stationId);
		rescuer.update();
	}

	@PostMapping(value = "/station/removeMember")
	void removeMember(Integer sessionId, Integer stationId, Integer accountId) {
		Session.authenticateAccount(sessionId, Station.load(stationId).getOwnerId());
		Rescuer rescuer = Rescuer.load(accountId);
		rescuer.setStationId(null);
		rescuer.update();
	}

	@GetMapping(value = "/station/listMembers")
	RescuerDTO[] listMembers(Integer sessionId, Integer stationId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		return Rescuer.loadAllForStation(stationId).stream().map(x -> new RescuerDTO(x)).toArray(RescuerDTO[]::new);
	}
}

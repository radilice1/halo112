import { Router } from "./router.js";
import { Session } from "./session.js";

import "./routesAccount.js";
import "./routesAdmin.js";
import "./routesRescuer.js";
import "./routesDispatcher.js";

Router.register(null, (query) => {
    if (!Session.checkLoggedIn()) return;

    if (Session.account.accountType === "ADMINISTRATOR") {
        Router.follow("#admin");
    } else if (Session.account.accountType === "DISPATCHER") {
        Router.follow("#dispatcher");
    } else {
        Router.follow("#rescuer");
    }
});

Router.apply();

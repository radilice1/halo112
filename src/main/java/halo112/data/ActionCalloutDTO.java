package halo112.data;

import halo112.model.AccountType;
import halo112.model.ActionCallout;
import halo112.model.MethodOfTransportation;
import halo112.model.UrgencyLevel;

public class ActionCalloutDTO {

	private int actionId;
	private AccountType accountType;
	private MethodOfTransportation methodOfTransportation;
	private UrgencyLevel urgency;

	public ActionCalloutDTO() {
	}
	
	public ActionCalloutDTO(ActionCallout callout) {
		actionId = callout.getActionId();
		accountType = callout.getAccountType();
		methodOfTransportation = callout.getMethodOfTransportation();
		urgency = callout.getUrgency();
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public MethodOfTransportation getMethodOfTransportation() {
		return methodOfTransportation;
	}

	public void setMethodOfTransportation(MethodOfTransportation methodOfTransportation) {
		this.methodOfTransportation = methodOfTransportation;
	}

	public UrgencyLevel getUrgency() {
		return urgency;
	}

	public void setUrgency(UrgencyLevel urgency) {
		this.urgency = urgency;
	}

}

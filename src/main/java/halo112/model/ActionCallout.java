package halo112.model;

import java.util.List;

import halo112.Database;
import halo112.data.ActionCalloutDTO;

public class ActionCallout {

	public static ActionCallout load(int actionId, MethodOfTransportation methodOfTransportation) {
		List<ActionCallout> list =  loadForAction(actionId);
		for(ActionCallout callout : list) {
			if(callout.getMethodOfTransportation().equals(methodOfTransportation)) return callout;
		}
		throw new RuntimeException("not callout with this action_id and vehicle_name");
	
	}

	public static List<ActionCallout> loadForAction(int actionId) {
		List<ActionCallout> result =
				Database.query("SELECT vid, aid, urgency, account_type_id FROM action_callout WHERE aid = ?", (stmt) -> {
			stmt.setInt(1, actionId);
		}, (row) -> {
			ActionCallout callout = new ActionCallout();
			callout.setMethodOfTransportation(MethodOfTransportation.values()[row.getInt(1)]);
			callout.setActionId(row.getInt(2));
			callout.setUrgency(UrgencyLevel.values()[row.getInt(3)]);
			callout.setAccountType(AccountType.values()[row.getInt(4)]);
			return callout;
		});
		return result;

	}

	public void store() {
		List<Integer> ids = Database.update("INSERT INTO Action_Callout"
				+ "(vid, aid, urgency, account_type_id) "
				+ "VALUES (?, ?, ?, ?)", (stmt) -> {
			stmt.setInt(1, methodOfTransportation.ordinal());
			stmt.setInt(2, actionId);
			stmt.setInt(3, urgency.ordinal());
			stmt.setInt(4, accountType.ordinal());			
		}, (r) -> r.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("failed to create callout");
	}

	public static void delete(ActionCalloutDTO dto) {
		Database.execute("DELETE FROM Action_Callout WHERE vid = ? AND aid = ? AND urgency = ? AND account_type_id = ?", (stmt) -> {
			stmt.setInt(1, dto.getMethodOfTransportation().ordinal());
			stmt.setInt(2, dto.getActionId());
			stmt.setInt(3, dto.getUrgency().ordinal());
			stmt.setInt(4, dto.getAccountType().ordinal());			
		});
	}

	private int actionId;
	private AccountType accountType;
	private MethodOfTransportation methodOfTransportation;
	private UrgencyLevel urgency;
	
	public ActionCallout() {
	}
	
	public ActionCallout(ActionCalloutDTO actionCalloutDTO) {
		this.actionId = actionCalloutDTO.getActionId();
		this.accountType = actionCalloutDTO.getAccountType();
		this.methodOfTransportation = actionCalloutDTO.getMethodOfTransportation();
		this.urgency = actionCalloutDTO.getUrgency();
	}
	

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public MethodOfTransportation getMethodOfTransportation() {
		return methodOfTransportation;
	}

	public void setMethodOfTransportation(MethodOfTransportation methodOfTransportation) {
		this.methodOfTransportation = methodOfTransportation;
	}

	public UrgencyLevel getUrgency() {
		return urgency;
	}

	public void setUrgency(UrgencyLevel urgency) {
		this.urgency = urgency;
	}

}

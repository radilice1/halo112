package halo112.model;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import halo112.Database;

public class Rescuer extends Account {

	private boolean available;
	private double longitude;
	private double latitude;
	private Integer stationId;
	private Integer actionId;
	private List<MethodOfTransportation> methodsOfTransportation = new ArrayList<MethodOfTransportation>();

	public static void deleteRescuerVehicles(int accountId) {
		Database.execute("DELETE FROM rescuer_vehicle WHERE id = ?", (stmt) -> {
			stmt.setInt(1, accountId);
		});
	}

	public static void setRescueVehicles(int accountId, MethodOfTransportation[] methods) {
		deleteRescuerVehicles(accountId);
		for (final MethodOfTransportation method : methods) {
			Database.execute("INSERT INTO rescuer_vehicle (id, vid) VALUES (?, ?)", (stmt) -> {
				stmt.setInt(1, accountId);
				stmt.setInt(2, method.ordinal());
			});
		}
	}

	public static Rescuer load(int id) {
		List<Rescuer> result = Database.query(
				"SELECT available, longitude, latitude, station_id, aid, account.id, username, password, email, first_name, last_name, phone_number, approved, account_type_id FROM account LEFT JOIN rescuer ON rescuer.id = account.id WHERE account_type_id IN (1, 2, 3) AND approved = TRUE AND account.id = ?",
				(stmt) -> {
					stmt.setInt(1, id);
				}, (row) -> {
					Rescuer rescuer = new Rescuer();
					rescuer.setAvailable(row.getBoolean(1));
					rescuer.setLongitude(row.getFloat(2));
					rescuer.setLatitude(row.getFloat(3));
					rescuer.setStationId(row.getInt(4));
					if (row.wasNull())
						rescuer.setStationId(null);
					rescuer.setActionId(row.getInt(5));
					if (row.wasNull())
						rescuer.setActionId(null);
					rescuer.setId(row.getInt(6));
					rescuer.setUsername(row.getString(7));
					rescuer.setPassword(row.getString(8));
					rescuer.setEmail(row.getString(9));
					rescuer.setFirstName(row.getString(10));
					rescuer.setLastName(row.getString(11));
					rescuer.setPhone(row.getString(12));
					rescuer.setApproved(row.getBoolean(13));
					rescuer.setAccountType(AccountType.values()[row.getInt(14)]);
					return rescuer;
				});
		if (result.size() != 1) {
			throw new RuntimeException("not rescuer with id " + id);
		}

		return result.get(0);
	}

	public static List<Rescuer> loadAllRescuers() {
		List<Rescuer> result = Database.query(
				"SELECT available, longitude, latitude, station_id, aid, account.id, username, password, email, first_name, last_name, phone_number, approved, account_type_id FROM account LEFT JOIN rescuer ON account.id = rescuer.id WHERE account_type_id IN (1, 2, 3) AND approved = TRUE ",
				null, (row) -> {
					Rescuer rescuer = new Rescuer();
					rescuer.setAvailable(row.getBoolean(1));
					rescuer.setLongitude(row.getFloat(2));
					rescuer.setLatitude(row.getFloat(3));
					rescuer.setStationId(row.getInt(4));
					if (row.wasNull())
						rescuer.setStationId(null);
					rescuer.setActionId(row.getInt(5));
					if (row.wasNull())
						rescuer.setActionId(null);
					rescuer.setId(row.getInt(6));
					rescuer.setUsername(row.getString(7));
					rescuer.setPassword(row.getString(8));
					rescuer.setEmail(row.getString(9));
					rescuer.setFirstName(row.getString(10));
					rescuer.setLastName(row.getString(11));
					rescuer.setPhone(row.getString(12));
					rescuer.setApproved(row.getBoolean(13));
					rescuer.setAccountType(AccountType.values()[row.getInt(14)]);
					return rescuer;
				});
		return result;
	}

	public static List<Rescuer> loadAllForStation(int stationId) {
		List<Rescuer> result = Database.query(
				"SELECT available, longitude, latitude, station_id, aid, account.id, username, password, email, first_name, last_name, phone_number, approved, account_type_id FROM account LEFT JOIN rescuer ON rescuer.id = account.id WHERE account_type_id IN (1, 2, 3) AND approved = TRUE AND station_id = ?",
				(stmt) -> {
					stmt.setInt(1, stationId);
				}, (row) -> {
					Rescuer rescuer = new Rescuer();
					rescuer.setAvailable(row.getBoolean(1));
					rescuer.setLongitude(row.getFloat(2));
					rescuer.setLatitude(row.getFloat(3));
					rescuer.setStationId(row.getInt(4));
					if (row.wasNull())
						rescuer.setStationId(null);
					rescuer.setActionId(row.getInt(5));
					if (row.wasNull())
						rescuer.setActionId(null);
					rescuer.setId(row.getInt(6));
					rescuer.setUsername(row.getString(7));
					rescuer.setPassword(row.getString(8));
					rescuer.setEmail(row.getString(9));
					rescuer.setFirstName(row.getString(10));
					rescuer.setLastName(row.getString(11));
					rescuer.setPhone(row.getString(12));
					rescuer.setApproved(row.getBoolean(13));
					rescuer.setAccountType(AccountType.values()[row.getInt(14)]);
					return rescuer;
				});
		return result;
	}

	@Override
	public void update() {
		Database.execute("INSERT INTO rescuer (available, longitude, latitude, station_id, id)"
				       + " VALUES (?, ?, ?, ?, ?)"
				       + " ON CONFLICT (id) DO UPDATE"
				       + " SET available = excluded.available,"
				       + "     longitude = excluded.longitude,"
				       + "     latitude = excluded.latitude,"
				       + "     station_id = excluded.station_id", (stmt) -> {
			stmt.setBoolean(1, available);
			stmt.setDouble(2, longitude);
			stmt.setDouble(3, latitude);
			if (stationId == null)
				stmt.setNull(4, Types.DOUBLE);
			else
				stmt.setInt(4, stationId);
			stmt.setInt(5, getId());
		});
	}

	public static void updateLocation(int accountId, double longitude, double latitude) {
		Database.execute("INSERT INTO rescuer (available, longitude, latitude, station_id, id)"
				       + " VALUES (FALSE, ?, ?, NULL, ?)"
				       + " ON CONFLICT (id) DO UPDATE"
				       + " SET longitude = excluded.longitude,"
				       + "     latitude = excluded.latitude", (stmt) -> {
			stmt.setDouble(1, longitude);
			stmt.setDouble(2, latitude);
			stmt.setInt(3, accountId);
		});
	}

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public List<MethodOfTransportation> getMethodsOfTransportation() {
		return methodsOfTransportation;
	}

}

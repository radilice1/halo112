package halo112.data;

import halo112.model.MethodOfTransportation;
import halo112.model.Rescuer;

public class RescuerDTO extends AccountDTO {

	private boolean available;
	private double longitude;
	private double latitude;
	private Integer stationId;
	private Integer actionId;
	private MethodOfTransportation[] methodsOfTransportation;

	public RescuerDTO(Rescuer rescuer) {
		super(rescuer);
		setAvailable(rescuer.isAvailable());
		setLongitude(rescuer.getLongitude());
		setLatitude(rescuer.getLatitude());
		setStationId(rescuer.getStationId());
		setActionId(rescuer.getActionId());
		setMethodsOfTransportation(rescuer.getMethodsOfTransportation().toArray(new MethodOfTransportation[0]));
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public Integer getStationId() {
		return stationId;
	}
	
	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}
	
	public Integer getActionId() {
		return actionId;
	}
	
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public MethodOfTransportation[] getMethodsOfTransportation() {
		return methodsOfTransportation;
	}

	public void setMethodsOfTransportation(MethodOfTransportation[] methodsOfTransportation) {
		this.methodsOfTransportation = methodsOfTransportation;
	}

}

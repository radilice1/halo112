package halo112.controllers;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import halo112.PrivilegeLevel;
import halo112.data.RescuerDTO;
import halo112.model.MethodOfTransportation;
import halo112.model.Rescuer;
import halo112.model.Session;

@RestController
public class RescuerController {

	@GetMapping(value = "/rescuer/list")
	RescuerDTO[] list(Integer sessionId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		return Rescuer.loadAllRescuers().stream().map(x -> new RescuerDTO(x)).toArray(RescuerDTO[]::new);
	}

	@PostMapping(value = "/rescuer/updateAvailable")
	void updateAvailable(Integer sessionId, Boolean available) {
		Session session = Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Rescuer updateRescuer = Rescuer.load(session.getAccountId());
		updateRescuer.setAvailable(available);
		updateRescuer.update();
	}

	@PostMapping(value = "/rescuer/updateLocation")
	void updateLocation(Integer sessionId, Double longitude, Double latitude) {
		Session session = Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Rescuer.updateLocation(session.getAccountId(), longitude, latitude);
	}

	@PostMapping(value = "/rescuer/updateMethodsOfTransportation")
	void updateMethodsOfTransportation(Integer sessionId, MethodOfTransportation[] methods) {
		Session session = Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Rescuer.setRescueVehicles(session.getAccountId(), methods);
	}

	@GetMapping(value = "/rescuer/route")
	String route(Integer sessionId, Integer accountId, Double longitude, Double latitude) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Rescuer rescuer = Rescuer.load(accountId);
		
		String url = "http://router.project-osrm.org/route/v1/car/"
				+ rescuer.getLongitude() + "," + rescuer.getLatitude() + ";"
				+ longitude + "," + latitude + ".json?overview=full";
		return new RestTemplateBuilder().build().getForObject(url, String.class);
	}

}

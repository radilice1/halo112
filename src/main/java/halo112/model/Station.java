package halo112.model;

import java.sql.Types;
import java.util.List;

import halo112.Database;

public class Station {
	public static Station load(int id) {
		List<Station> result = Database.query("SELECT station_id, station_name, leader_id FROM station WHERE station_id = ?", (stmt) -> {
			stmt.setInt(1, id);
		}, (row) -> {
			Station station = new Station();
			station.setId(row.getInt(1));
			station.setName(row.getString(2));
			station.setOwnerId(row.getInt(3));
			if (row.wasNull())
				station.setOwnerId(null);
			return station;
		});
		if (result.size() != 1)
			throw new RuntimeException("No station with id: " + id);

		return result.get(0);
	}

	public static List<Station> loadAll() {
		List<Station> result = Database.query("SELECT station_id, station_name, leader_id FROM station", (row) -> {
			Station station = new Station();
			station.setId(row.getInt(1));
			station.setName(row.getString(2));
			station.setOwnerId(row.getInt(3));
			if (row.wasNull())
				station.setOwnerId(null);
			return station;
		});
		
		return result;
	}

	public void store() {
		List<Integer> ids = Database.update("INSERT INTO station(station_name, leader_id) VALUES (?, ?)", (stmt) -> {
			stmt.setString(1, name);
			if (ownerId == null)
				stmt.setNull(2, Types.INTEGER);
			else
				stmt.setInt(2, ownerId);	
		}, (row) -> row.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("Failed to create a station");
		setId(ids.get(0));
	}
	
	public void update() {
		List<Integer> ids = Database.update("UPDATE station SET station_name = ?, leader_id = ? WHERE station_id = ?", (stmt) -> {
			stmt.setString(1, name);
			if (ownerId == null)
				stmt.setNull(2, Types.INTEGER);
			else
				stmt.setInt(2, ownerId);
			stmt.setInt(3, id);	
		}, (row) -> row.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("Failed to create a station");
		setId(ids.get(0));
	}
	
	public static void delete(int id) {
		Database.execute("UPDATE rescuer SET station_id = NULL where station_id = ?", (stmt) -> {
			stmt.setInt(1, id);
		});
		Database.execute("DELETE FROM station WHERE station_id = ?", (stmt) -> {
			stmt.setInt(1, id);
		});
	}

	private int id;
	private String name;
	private Integer ownerId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

}

package halo112.model;

import java.util.List;

import halo112.Database;
import halo112.data.ActionCommentDTO;

public class ActionComment {

	private int id;
	private int actionId;
	private int posterAccountId;
	private double longitude;
	private double latitude;
	private String text;

	public static ActionComment load(int id) {
		List<ActionComment> result = Database.query("SELECT text, acid, latitude, longitude, aid, id  FROM Action_Comment WHERE acid = ?", (stmt) -> {
			stmt.setInt(1, id);
		}, (row) -> {
			ActionComment comment = new ActionComment();
			comment.setText(row.getString(1));
			comment.setId(row.getInt(2));
			comment.setLatitude(row.getDouble(3));
			comment.setLongitude(row.getDouble(4));
			comment.setActionId(row.getInt(5));
			comment.setPosterAccountId(row.getInt(6));
			return comment;
		});
		if (result.size() != 1)
			throw new RuntimeException("not action_comment with id " + id);
		return result.get(0);
	}

	public static List<ActionComment> loadForAction(int actionId) {
		List<ActionComment> result = Database.query("SELECT text, acid, latitude, longitude, aid, id FROM Action_Comment WHERE aid = ?", (stmt) -> {
			stmt.setInt(1, actionId);
		}, (row) -> {
			ActionComment comment = new ActionComment();
			comment.setText(row.getString(1));
			comment.setId(row.getInt(2));
			comment.setLatitude(row.getDouble(3));
			comment.setLongitude(row.getDouble(4));
			comment.setActionId(row.getInt(5));
			comment.setPosterAccountId(row.getInt(6));
			return comment;
		});
		return result;
	}

	public void store() {
		Database.execute("INSERT INTO Action_Comment(text, latitude, longitude, aid, id) VALUES (?, ?, ?, ?, ?)", (stmt) -> {
			stmt.setString(1, text);
			stmt.setDouble(2, latitude);
			stmt.setDouble(3, longitude);
			stmt.setInt(4, actionId);
			stmt.setInt(5, posterAccountId);
		});
	}
	
	public static void delete(int id) { 
		Database.execute("DELETE FROM Action_Comment WHERE ACID = ?", (stmt) -> {
			stmt.setInt(1, id);
		});
	}
	
	public ActionComment() {
	}
	
	public ActionComment(ActionCommentDTO commentDTO) {
		this.id = commentDTO.getId();
		this.actionId = commentDTO.getActionId();
		this.latitude = commentDTO.getLatitude();
		this.longitude = commentDTO.getLongitude();
		this.posterAccountId = commentDTO.getPosterAccountId();
		this.text = commentDTO.getText();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public int getPosterAccountId() {
		return posterAccountId;
	}

	public void setPosterAccountId(int posterAccountId) {
		this.posterAccountId = posterAccountId;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

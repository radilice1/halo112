package halo112.data;

import halo112.model.Task;

public class TaskDTO {

	private int id;
	private int actionId;
	private int assignedAccountId;
	private String route;
	private TaskCommentDTO[] comments;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public int getAssignedAccountId() {
		return assignedAccountId;
	}

	public void setAssignedAccountId(int assignedAccountId) {
		this.assignedAccountId = assignedAccountId;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public TaskCommentDTO[] getComments() {
		return comments;
	}

	public void setComments(TaskCommentDTO[] comments) {
		this.comments = comments;
	}
	
	public TaskDTO() {
	}
	public TaskDTO(Task task) {
		this.actionId = task.getActionId();
		this.assignedAccountId = task.getAssignedAccountId();
		this.id = task.getId();
		this.route = task.getRoute();
	}

}

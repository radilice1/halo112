import { Router } from "./router.js";
import { Session } from "./session.js";

let stationList = null;
let rescuerList = null;
let actionList = null;
let currentAction = null;
let map = null;

function downsampleImage(data, maximumSize, callback) {
    let img = document.createElement("img");
    img.onload = function(event) {
        let scale = 1;
        if (img.width  * scale > maximumSize) scale = maximumSize / (img.width  * scale);
        if (img.height * scale > maximumSize) scale = maximumSize / (img.height * scale);
        let newWidth  = Math.floor(img.width  * scale);
        let newHeight = Math.floor(img.height * scale);

        let canvas = document.createElement("canvas");
        canvas.width = newWidth;
        canvas.height = newHeight;
        let context = canvas.getContext("2d");
        context.drawImage(img, 0, 0, img.width, img.height, 0, 0, newWidth, newHeight);
        callback(canvas.toDataURL("image/png"));
    }
    img.src = data;
}

function openTaskComments(task) {
    for (let element of document.querySelectorAll(".edit-form-container"))
        element.remove();
    let templateDiv = Router.appendContent(Router.bodyContent, Router.template("task-comments"));
    templateDiv.setAttribute("data-task-comments-id", task.id);

    function addComment(comment) {
        let poster = "Dispečer";
        for (let rescuer of rescuerList)
            if (rescuer.id == comment.posterAccountId)
                poster = rescuer.firstName + " " + rescuer.lastName;

        let commentsDiv = document.getElementById("task-comments-div");
        let commentDiv = Router.appendContent(commentsDiv, Router.template("task-comment"))
        commentDiv.querySelector(".poster").textContent = poster;
        commentDiv.querySelector(".content").textContent = comment.text;

        commentDiv.querySelector(".deleteButton").addEventListener("click", async () => {
            if (!comment.id) return;
            commentDiv.remove();
            await fetch("/action/deleteTaskComment?" + new URLSearchParams({
                sessionId: Session.id,
                commentId: comment.id
            }), { method: "post" });
            refreshActionList();
        });
    }

    let comments = task.comments;
    comments.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let comment of comments)
        addComment(comment);

    document.getElementById("edit-form").addEventListener("submit", (event) => {
        event.preventDefault();
        let comment = {
            id: 0,
            taskId: task.id,
            text: document.getElementById("task-comment-text").value
        };

        fetch("/action/addTaskComment?" + new URLSearchParams({ sessionId: Session.id, taskId: task.id }), {
            method: "post",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(comment)
        })
        .then((response) => refreshActionList())
        .catch((error) => console.error(error))

        addComment(comment);
        document.getElementById("task-comment-text").value = "";
        return false;
    });

    document.getElementById("edit-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".edit-form-container"))
            element.remove();
    });
}

let commentOnMapMarker = null;
let commentOnMapMarkerId = null;
let routeOnMap = null;
let routeOnMapId = null;

function setCurrentAction(action) {
    for (let element of document.querySelectorAll(".action-panel"))
        element.remove();
    currentAction = action;
    if (!action) return;

    Router.appendContent(document.querySelector(".sidebar"), Router.template("action"));

    document.getElementById("action-info").textContent = action.info;

    let images = action.images;
    images.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let image of images) {
        let img = document.createElement("img");
        img.src = atob(image.image);
        document.getElementById("action-images-div").appendChild(img);
    }

    function addComment(comment) {
        let poster = "Dispečer";
        for (let rescuer of rescuerList)
            if (rescuer.id == comment.posterAccountId)
                poster = rescuer.firstName + " " + rescuer.lastName;

        let commentsDiv = document.getElementById("action-comments-div");
        let commentDiv = Router.appendContent(commentsDiv, Router.template("action-comment"))
        commentDiv.querySelector(".poster").textContent = poster;
        commentDiv.querySelector(".content").textContent = comment.text;

        commentDiv.querySelector(".deleteButton").addEventListener("click", () => {
            if (!comment.id) return;
            commentDiv.remove();
            fetch("/action/deleteActionComment?" + new URLSearchParams({ sessionId: Session.id, commentId: comment.id }), { method: "post" })
        });

        commentDiv.querySelector(".showOnMapButton").addEventListener("click", () => {
            if (commentOnMapMarker) {
                map.removeLayer(commentOnMapMarker);
                commentOnMapMarker = null;
            }

            if (commentOnMapMarkerId == comment.id) {
                commentOnMapMarkerId = null;
                return;
            }

            commentOnMapMarkerId = comment.id;
            commentOnMapMarker = L.marker([ comment.latitude, comment.longitude ], {
                icon: L.AwesomeMarkers.icon({ icon: "comment", markerColor: "gray" })
            }).addTo(map).bindPopup(comment.text).openPopup();
        });
    }

    let comments = action.comments;
    comments.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let comment of comments)
        addComment(comment);

    function addCallout(callout) {
        let role = "<nepoznata uloga>";
        if (callout.accountType == "DOCTOR") role = "Doktor";
        if (callout.accountType == "FIREFIGTHER") role = "Vatrogasac";
        if (callout.accountType == "POLICE") role = "Policajac";
        let vehicle = "<nepoznato vozilo>";
        if (callout.methodOfTransportation == "MOTORCYCLE")      vehicle = "Motocikl";
        if (callout.methodOfTransportation == "AMBULANCE")       vehicle = "Hitna pomoć";
        if (callout.methodOfTransportation == "TANK_TRUCK")      vehicle = "Autocisterna";
        if (callout.methodOfTransportation == "CAR_LADDERS")     vehicle = "Autoljestve";
        if (callout.methodOfTransportation == "COMMAND_VEHICLE") vehicle = "Zapovjedno vozilo";
        if (callout.methodOfTransportation == "FOREST_VEHICLE")  vehicle = "Šumsko vozilo";
        if (callout.methodOfTransportation == "PEDESTRIAN")      vehicle = "Pješak";
        if (callout.methodOfTransportation == "CAR")             vehicle = "Automobil";
        if (callout.methodOfTransportation == "ARMORED_VEHICLE") vehicle = "Oklopno vozilo";
        let urgency = "<nepoznata hitnost>";
        if (callout.urgency == "LOW") urgency = "Niska";
        if (callout.urgency == "MEDIUM") urgency = "Srednja";
        if (callout.urgency == "HIGH") urgency = "Visoka";
        if (callout.urgency == "VERY_HIGH") urgency = "Vrlo visoka";

        let p = document.createElement("p");
        p.textContent = role + ", " + vehicle + ", " + urgency + " hitnost";
        document.getElementById("action-callouts-div").appendChild(p);
    }
    for (let callout of action.callouts)
        addCallout(callout);


    function showTaskRoute(task) {
        if (routeOnMap) {
            map.removeLayer(routeOnMap);
            routeOnMap = null;
        }

        if (routeOnMapId == task.id) {
            routeOnMapId = null;
            return;
        }

        routeOnMapId = task.id;
        routeOnMap = new L.Polyline.fromEncoded(task.route, {
            color: "red",
            weight: 3,
            opacity: 0.5,
            smoothFactor: 1
        });
        routeOnMap.addTo(map);
    }

    let tasks = action.tasks;
    tasks.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let task of tasks)
        if (document.querySelectorAll("[data-task-comments-id='" + task.id + "']").length)
            openTaskComments(task);
    for (let rescuerId of action.rescuers) {
        let rescuer = rescuerList.find(x => x.id == rescuerId);
        if (!rescuer) continue;

        let rescuersDiv = document.getElementById("action-rescuers-div");
        let rescuerDiv = Router.appendContent(rescuersDiv, Router.template("action-rescuer"))
        rescuerDiv.querySelector(".name").textContent = rescuer.firstName + " " + rescuer.lastName;

        rescuerDiv.querySelector(".deleteButton").addEventListener("click", () => {
            rescuerDiv.remove();
            fetch("/action/removeRescuer?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id, accountId: rescuer.id }), { method: "post" })
        });

        function addTask(task) {
            let tasksDiv = rescuerDiv.querySelector(".rescuer-tasks");
            let taskDiv = Router.appendContent(tasksDiv, Router.template("action-rescuer-task"))
            taskDiv.querySelector(".taskIndex").textContent = tasksDiv.querySelectorAll(".action-rescuer-task").length;
            taskDiv.querySelector(".showOnMapButton").addEventListener("click", () => showTaskRoute(task));
            taskDiv.querySelector(".commentButton").addEventListener("click", () => {
                openTaskComments(task);
            });
        }

        let myTasks = tasks.filter(x => x.assignedAccountId == rescuer.id);
        for (let task of myTasks)
            addTask(task);

        rescuerDiv.querySelector(".addTaskButton").addEventListener("click", async () => {
            let destination = await pickLocation();
            let response = await fetch("/rescuer/route?" + new URLSearchParams({
                sessionId: Session.id,
                accountId: 20,
                longitude: destination.lng,
                latitude: destination.lat
            }), { method: "get" });
            let osrmData = await response.json();
            let task = {
                id: 0,
                actionId: action.id,
                assignedAccountId: rescuer.id,
                route: osrmData.routes[0].geometry,
                comments: []
            };

            showTaskRoute(task);
            addTask(task);
            await fetch("/action/addTask?" + new URLSearchParams({ sessionId: Session.id }), {
                method: "post",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(task)
            });

            refreshActionList();
        });
    }


    document.querySelector(".action-image-upload").addEventListener("click", (event) => {
        document.querySelector(".action-image-file").click();
    });
    document.querySelector(".action-image-file").addEventListener("change", (event) => {
        for (let file of event.srcElement.files) {
            let reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = function(event) {
                let dataUrl = `data:${file.type};base64,${btoa(event.target.result)}`;
                downsampleImage(dataUrl, 300, (downsampledDataUrl) => {
                    let img = document.createElement("img");
                    img.setAttribute("src", downsampledDataUrl);
                    document.getElementById("action-images-div").appendChild(img);

                    fetch("/action/addActionImage?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id }), {
                        method: "post",
                        body: btoa(downsampledDataUrl)
                    })
                    .then((response) => refreshActionList())
                    .catch((error) => console.error(error))
                });
            };
            reader.onerror = function() {
                console.error(reader.error);
            };
        }
    });

    document.getElementById("action-comment-form").addEventListener("submit", (event) => {
        event.preventDefault();

        let comment = {
            id: 0,
            actionId: action.id,
            posterAccountId: Session.account.id,
            text: document.getElementById("comment-text").value,
            latitude: action.latitude,
            longitude: action.longitude
        };

        fetch("/action/addActionComment?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id }), {
            method: "post",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(comment)
        })
        .then((response) => refreshActionList())
        .catch((error) => console.error(error))

        addComment(comment);
        document.getElementById("comment-text").value = "";
        return false;
    });

    function filterVehicles() {
        let role = document.getElementById("callout-role").value;
        let currentChoice = document.getElementById("callout-vehicle").value;
        let validChoice = null;
        let currentlyInvalid = false;
        for (let option of document.querySelectorAll("#callout-vehicle option")) {
            let enabled = option.getAttribute("data-for") == role || option.getAttribute("data-for2") == role;
            option.style.display = enabled ? "block" : "none";

            let value = option.getAttribute("value");
            if (currentChoice == value && !enabled) currentlyInvalid = true;
            if (!validChoice && enabled) validChoice = value;
        }
        if (currentlyInvalid)
            document.getElementById("callout-vehicle").value = validChoice;
    }
    filterVehicles();
    document.getElementById("callout-role").addEventListener("change", filterVehicles);
    document.getElementById("action-callout-form").addEventListener("submit", (event) => {
        event.preventDefault();

        let callout = {
            actionId: action.id,
            accountType: document.getElementById("callout-role").value,
            methodOfTransportation: document.getElementById("callout-vehicle").value,
            urgency: document.getElementById("callout-urgency").value
        };

        fetch("/action/addCallout?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id }), {
            method: "post",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(callout)
        })
        .then((response) => refreshActionList())
        .catch((error) => console.error(error))

        addCallout(callout);
        return false;
    });

    document.getElementById("close-action-button").addEventListener("click", (event) => {
        askForConfirmation("Ova radnja će zatvoriti trenutačno odabranu akciju.", () => {
            fetch("/action/delete?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id }), {
                method: "post"
            })
            .then((response) => refreshActionList())
            .catch((error) => console.error(error));

            setCurrentAction(null);
            if (action.marker)
                map.removeLayer(action.marker);
            updateVoronoi();
        });
    });
}

function askForConfirmation(action, callback) {
    for (let element of document.querySelectorAll(".confirm-form-container"))
        element.remove();
    Router.appendContent(Router.bodyContent, Router.template("confirm"));

    document.querySelector(".action-description").textContent = action;

    document.getElementById("confirm-form").addEventListener("submit", (event) => {
        for (let element of document.querySelectorAll(".confirm-form-container"))
            element.remove();
        callback();
    });

    document.getElementById("confirm-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".confirm-form-container"))
            element.remove();
    });
}

function updateStationCounters() {
    for (let station of stationList) {
        let p = document.querySelector("#available-rescuer-by-station [data-station='" + station.id + "']");
        if (!p) {
            p = document.createElement("p");
            p.setAttribute("data-station", station.id);
            document.getElementById("available-rescuer-by-station").appendChild(p);
        }
        let count = rescuerList.filter(x => x.stationId == station.id && x.longitude && x.latitude && x.available).length;
        p.textContent = station.name + ": " + count;
    }
}

let voronoiLayers = [];

function updateVoronoi() {
    if (!map)
        return;
    for (let layer of voronoiLayers)
        map.removeLayer(layer);
    voronoiLayers = [];
    if (!document.getElementById("dispatcher-voronoi").checked)
        return;

    let data = [];
    for (let rescuer of rescuerList) {
        if (!rescuer.marker) continue;
        let color = "gray";
        if (rescuer.accountType === "DOCTOR")      color = "green";
        if (rescuer.accountType === "FIREFIGTHER") color = "orange";
        if (rescuer.accountType === "POLICE")      color = "blue";
        data.push({ lng: rescuer.longitude, lat: rescuer.latitude, color: color });
    }

    for (let poly of d3.geom.voronoi()
        .x(function(d) { return d.lat; })
        .y(function(d) { return d.lng; })
        .clipExtent([[ 45.815399 - 0.5, 15.966568 - 0.5 ], [ 45.815399 + 0.5, 15.966568 + 0.5 ]])
        (data)) {
        let coords = [];
        for (let i = 0; i < poly.length; i++)
            coords.push(poly[i]);
        voronoiLayers.push(L.polygon(coords, { color: poly.point.color }).addTo(map));
    }
}

function updateRescuerOnMap(rescuer) {
    let location = new L.LatLng(rescuer.latitude, rescuer.longitude);
    if (rescuer.marker) {
        rescuer.marker.setLatLng(location);
    } else {
        let color = "gray";
        if (rescuer.accountType === "DOCTOR")      color = "green";
        if (rescuer.accountType === "FIREFIGTHER") color = "orange";
        if (rescuer.accountType === "POLICE")      color = "blue";

        rescuer.marker = L.marker(location, {
            icon: L.AwesomeMarkers.icon({ icon: "user", markerColor: color })
        }).addTo(map).bindPopup(rescuer.firstName + " " + rescuer.lastName);
    }
    updateVoronoi();
}

function updateActionOnMap(action) {
    let location = new L.LatLng(action.latitude, action.longitude);
    if (action.marker) {
        action.marker.setLatLng(location);
    } else {
        action.marker = L.marker(location, {
            icon: L.AwesomeMarkers.icon({ icon: "exclamation", markerColor: "red" })
        }).addTo(map).on("click", function(event) {
            setCurrentAction(action);
        });
    }
}

function refreshActionList() {
    fetch("/action/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" })
    .then(async (response) => {
        if (response.status != 200) return;
        let currentActionFound = false;
        let cookie = Symbol();
        for (let updatedAction of await response.json()) {
            let action = null;
            for (let existingAction of actionList) {
                if (existingAction.id != updatedAction.id) continue;
                for (let key in updatedAction)
                    existingAction[key] = updatedAction[key];
                action = existingAction;
                break;
            }
            if (!action) {
                actionList.push(updatedAction);
                action = updatedAction;
            }
            action.cookie = cookie;
            if (!currentAction || action.id == currentAction.id)
                currentActionFound = true;

            updateActionOnMap(action);
        }
        if (!currentActionFound) {
            setCurrentAction(null);
        } else {
            setCurrentAction(currentAction);
        }
        for (let removedAction of actionList.filter(x => x.cookie != cookie))
            if (removedAction.marker)
                map.removeLayer(removedAction.marker);
        updateVoronoi();
        actionList = actionList.filter(x => x.cookie == cookie);
    });
}

function showMap() {
    map = L.map("map").setView([45.815399, 15.966568], 13);
    window.map = map;
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    let bounds = L.latLngBounds(L.latLng(45.815399 - 0.5, 15.966568 - 0.5),
                                L.latLng(45.815399 + 0.5, 15.966568 + 0.5));
    map.setMaxBounds(bounds);
    map.on('drag', () => {
        map.panInsideBounds(bounds, { animate: false });
    });

    let poll = () => {
        fetch("/rescuer/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" })
        .then(async (response) => {
            if (response.status != 200) return;
            let cookie = Symbol();
            for (let updatedRescuer of await response.json()) {
                let rescuer = null;
                for (let existingRescuer of rescuerList) {
                    if (existingRescuer.id == updatedRescuer.id) {
                        rescuer = existingRescuer;
                        break;
                    }
                }
                if (!rescuer) {
                    rescuerList.push(updatedRescuer);
                    rescuer = updatedRescuer;
                }
                rescuer.cookie = cookie;

                rescuer.longitude = updatedRescuer.longitude;
                rescuer.latitude  = updatedRescuer.latitude;
                rescuer.available = updatedRescuer.available;
                if (rescuer.available && rescuer.latitude && rescuer.longitude) {
                    updateRescuerOnMap(rescuer);
                }
                else if (rescuer.marker) {
                    map.removeLayer(rescuer.marker);
                    rescuer.marker = null;
                }
            }
            for (let removedRescuer of rescuerList.filter(x => x.cookie != cookie))
                if (removedRescuer.marker)
                    map.removeLayer(removedRescuer.marker);
            updateVoronoi();
            rescuerList = rescuerList.filter(x => x.cookie == cookie);
            updateStationCounters();

            setTimeout(poll, 5000);
        })
        .catch((error) => {
            setTimeout(poll, 5000);
        });
    };
    poll();
    refreshActionList();
}

function pickLocation() {
    return new Promise((resolve, reject) => {
        let element = document.getElementById("map");
        let oldStyle = element.getAttribute("style");
        element.setAttribute("style", "outline: none");
        element.classList.add("pickingLocation");
        setTimeout(() => { map.invalidateSize() }, 50);
        setTimeout(() => { map.invalidateSize() }, 100);
        setTimeout(() => { map.invalidateSize() }, 500);

        let clickHandler = function(e) {
            map.off('click', clickHandler);
            resolve(e.latlng);

            element.setAttribute("style", oldStyle);
            element.classList.remove("pickingLocation");
            setTimeout(() => { map.invalidateSize() }, 50);
            setTimeout(() => { map.invalidateSize() }, 100);
            setTimeout(() => { map.invalidateSize() }, 500);
        };
        map.on('click', clickHandler);
    });
}

function openAction(location) {
    for (let element of document.querySelectorAll(".edit-form-container"))
        element.remove();
    Router.appendContent(Router.bodyContent, Router.template("dispatcher-create-action"));

    document.getElementById("edit-form").addEventListener("submit", (event) => {
        event.preventDefault();

        let action = {
            id: 0,
            creatorAccountId: Session.account.id,
            longitude: location.lng,
            latitude:  location.lat,
            info:      document.getElementById("info").value,
            tasks:     [],
            callouts:  [],
            comments:  [],
            images:    [],
            rescuers:  []
        };
        for (let img of document.querySelectorAll("#edit-form .action-images img"))
            action.images.push({ id: 0, image: btoa(img.getAttribute("src")) });

        askForConfirmation("Ova radnja će otvoriti novu akciju.", () => {
            for (let element of document.querySelectorAll(".edit-form-container"))
                element.remove();

            try {
                fetch("/action/create?" + new URLSearchParams({ sessionId: Session.id }), {
                    method: "post",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify(action)
                })
                .then((response) => refreshActionList())
                .catch((error) => console.error(error))
            } catch(error) {
                console.error(error);
            }
        });
        return false;
    });

    document.getElementById("edit-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".edit-form-container"))
            element.remove();
    });

    document.getElementById("action-image-upload").addEventListener("click", (event) => {
        document.getElementById("action-image-file").click();
    });
    document.getElementById("action-image-file").addEventListener("change", (event) => {
        for (let file of event.srcElement.files) {
            let reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = function(event) {
                let dataUrl = `data:${file.type};base64,${btoa(event.target.result)}`;
                downsampleImage(dataUrl, 300, (downsampledDataUrl) => {
                    let img = document.createElement("img");
                    img.setAttribute("src", downsampledDataUrl);
                    document.querySelector("#edit-form .action-images").appendChild(img);
                });
            };
            reader.onerror = function() {
                console.error(reader.error);
            };
        }
    });
}

Router.register("#dispatcher", (query) => {
    if (!Session.checkLoggedIn()) return;
    L.AwesomeMarkers.Icon.prototype.options.prefix = "fa";
    Router.replaceContent(Router.bodyContent, Router.template("dispatcher"));
    document.getElementById("logout-button").addEventListener("click", () => Session.logout());

    let accountButton = document.getElementById("account-button");
    accountButton.textContent = Session.account.firstName + " " + Session.account.lastName;

    stationList = [];
    rescuerList = [];
    actionList  = [];

    showMap();

    document.getElementById("start-action-button").addEventListener("click", async () => {
        let location = await pickLocation();
        openAction(location);
    });

    document.getElementById("dispatcher-voronoi").addEventListener("change", updateVoronoi);

    fetch("/station/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" })
    .then(async (response) => {
        stationList = await response.json();
        updateStationCounters();
    });
});

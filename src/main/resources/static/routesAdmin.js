import { Router } from "./router.js";
import { Session } from "./session.js";

function askForConfirmation(action, callback) {
    for (let element of document.querySelectorAll(".confirm-form-container"))
        element.remove();
    Router.appendContent(Router.bodyContent, Router.template("confirm"));

    document.querySelector(".action-description").textContent = action;

    document.getElementById("confirm-form").addEventListener("submit", (event) => {
        for (let element of document.querySelectorAll(".confirm-form-container"))
            element.remove();
        callback();
    });

    document.getElementById("confirm-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".confirm-form-container"))
            element.remove();
    });
}

function openEditPopup(account) {
    for (let element of document.querySelectorAll(".edit-form-container"))
        element.remove();
    Router.appendContent(Router.bodyContent, Router.template("admin-edit-account"));

    document.getElementById("role").value       = account.accountType;
    document.getElementById("username").value   = account.username;
    document.getElementById("password").value   = account.password;
    document.getElementById("first-name").value = account.firstName;
    document.getElementById("last-name").value  = account.lastName;
    document.getElementById("phone").value      = account.phone;
    document.getElementById("email").value      = account.email;

    let profilePictureButton = document.getElementById("profile-picture-button");
    let profilePictureInput  = document.getElementById("profile-picture");

    profilePictureButton.addEventListener("click", (event) => {
        profilePictureInput.click();
    });

    profilePictureInput.addEventListener("change", (event) => {
        for (let file of event.srcElement.files) {
            let reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = function(event) {
                let profilePicture = `data:${file.type};base64,${btoa(event.target.result)}`;
                profilePictureButton.style.backgroundImage = `url("${profilePicture}")`;
                account.profilePicture = btoa(profilePicture);
            };
            reader.onerror = function() {
                console.error(reader.error);
            };
        }
    });

    profilePictureButton.style.backgroundImage = `url("${atob(account.profilePicture)}")`;

    document.getElementById("edit-form").addEventListener("submit", (event) => {
        event.preventDefault();

        account.accountType = document.getElementById("role").value;
        account.username    = document.getElementById("username").value;
        account.password    = document.getElementById("password").value;
        account.firstName   = document.getElementById("first-name").value;
        account.lastName    = document.getElementById("last-name").value;
        account.phone       = document.getElementById("phone").value;
        account.email       = document.getElementById("email").value;

        askForConfirmation("Ovom radnjom ćete urediti podatke za račun: " + account.firstName + " " + account.lastName, () => {
            for (let element of document.querySelectorAll(".edit-form-container"))
                element.remove();

            try {
                fetch("/account/update?" + new URLSearchParams({ sessionId: Session.id }), {
                    method: "post",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify(account)
                })
                .then((response) => refreshAccountList())
                .catch((error) => console.error(error))
            } catch(error) {
                console.error(error);
            }
        });
        return false;
    });

    document.getElementById("edit-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".edit-form-container"))
            element.remove();
    });
}

function openStationEditPopup(station) {
    for (let element of document.querySelectorAll(".edit-form-container"))
        element.remove();
    Router.appendContent(Router.bodyContent, Router.template("admin-edit-station"));

    document.getElementById("name").value = station.name;

    for (let account of adminAccountList) {
        if (account.accountType === "ADMINISTRATOR") continue;
        if (account.accountType === "DISPATCHER")    continue;

        let accountTypeName = undefined;
        if (account.accountType === "DOCTOR")        accountTypeName = "Doktor";
        if (account.accountType === "FIREFIGTHER")   accountTypeName = "Vatrogasac";
        if (account.accountType === "POLICE")        accountTypeName = "Policajac";

        let option = document.createElement("option");
        option.setAttribute("value", account.id);
        option.textContent = account.firstName + " " + account.lastName + " (" + accountTypeName + ")";
        document.getElementById("owner").appendChild(option);
    }
    if (station.ownerId !== null) {
        document.getElementById("owner").value = station.ownerId;
    }

    document.getElementById("edit-form").addEventListener("submit", (event) => {
        event.preventDefault();

        let creating = (station.id === null);
        if (creating) station.id = 0;

        station.name = document.getElementById("name").value;

        let ownerId = document.getElementById("owner").value;
        station.ownerId = ownerId === "null" ? null : parseInt(ownerId);

        let message = creating
                    ? "Ovom radnjom ćete stvoriti stanicu: "
                    : "Ovom radnjom ćete urediti podatke za stanicu: ";
        askForConfirmation(message + station.name, () => {
            for (let element of document.querySelectorAll(".edit-form-container"))
                element.remove();

            try {
                let endpoint = (creating ? "/station/create" : "/station/update");
                fetch(endpoint + "?" + new URLSearchParams({ sessionId: Session.id }), {
                    method: "post",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify(station)
                })
                .then((response) => refreshStationList())
                .catch((error) => console.error(error))
            } catch(error) {
                console.error(error);
            }
        });
        return false;
    });

    document.getElementById("edit-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".edit-form-container"))
            element.remove();
    });
}

let adminAccountList = [];

function refreshAccountList() {
    let unregisteredList = document.getElementById("unregistered-list");
    let registeredList = document.getElementById("registered-list");

    fetch("/account/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" })
    .then(async (response) => {
        if (response.status == 200) {
            for (let account of document.querySelectorAll(".account")) {
                account.remove();
            }

            let accounts = await response.json();
            adminAccountList = accounts;
            for (let account of accounts) {
                let template = Router.template(account.approved ? "admin-registered-account" : "admin-unregistered-account");
                let accountDiv = Router.appendContent(account.approved ? registeredList : unregisteredList, template);

                let accountTypeName = undefined;
                if (account.accountType === "ADMINISTRATOR") accountTypeName = "Administrator";
                if (account.accountType === "DOCTOR")        accountTypeName = "Doktor";
                if (account.accountType === "FIREFIGTHER")   accountTypeName = "Vatrogasac";
                if (account.accountType === "POLICE")        accountTypeName = "Policajac";
                if (account.accountType === "DISPATCHER")    accountTypeName = "Dispečer";

                accountDiv.querySelector(".profile-image").style.backgroundImage = `url("${atob(account.profilePicture)}")`;
                accountDiv.querySelector(".profile-short-info").textContent = account.firstName + " " + account.lastName + " (" + accountTypeName + ")";

                if (account.approved) {
                    accountDiv.querySelector(".edit").addEventListener("click", (event) => {
                        openEditPopup(account);
                    });
                } else {
                    accountDiv.querySelector(".approve").addEventListener("click", (event) => {
                        askForConfirmation("Ovom radnjom ćete potvrditi prijavu registracije: " + account.firstName + " " + account.lastName, () => {
                            fetch("/account/approveRegistration?" + new URLSearchParams({ sessionId: Session.id, accountId: account.id }), { method: "post" })
                            .then((response) => refreshAccountList())
                            .catch((error) => console.error(error));
                        });
                    });
                }

                accountDiv.querySelector(".delete").addEventListener("click", (event) => {
                    askForConfirmation("Ovom radnjom ćete izbrisati račun: " + account.firstName + " " + account.lastName, () => {
                        accountDiv.remove();
                        adminAccountList.splice(adminAccountList.indexOf(account), 1);
                        fetch("/account/delete?" + new URLSearchParams({ sessionId: Session.id, accountId: account.id }), { method: "post" })
                        .catch((error) => console.error(error));
                    });
                });
            }
        } else {
            Session.removeFromStorage();
        }
    })
    .catch((error) => {
        console.error(error);
        Session.removeFromStorage();
    })
}

function refreshStationList() {
    let stationList = document.getElementById("station-list");

    fetch("/station/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" })
    .then(async (response) => {
        if (response.status == 200) {
            for (let station of document.querySelectorAll(".station")) {
                station.remove();
            }

            let stations = await response.json();
            for (let station of stations) {
                let template = Router.template("admin-station");
                let stationDiv = Router.appendContent(stationList, template);

                stationDiv.querySelector(".station-short-info").textContent = station.name;

                stationDiv.querySelector(".edit").addEventListener("click", (event) => {
                    openStationEditPopup(station);
                });

                stationDiv.querySelector(".delete").addEventListener("click", (event) => {
                    askForConfirmation("Ovom radnjom ćete izbrisati stanicu: " + station.name, () => {
                        stationDiv.remove();
                        fetch("/station/delete?" + new URLSearchParams({ sessionId: Session.id, stationId: station.id }), { method: "post" })
                        .catch((error) => console.error(error));
                    });
                });
            }
        } else {
            Session.removeFromStorage();
        }
    })
    .catch((error) => {
        console.error(error);
        Session.removeFromStorage();
    })
}

Router.register("#admin", (query) => {
    if (!Session.checkLoggedIn()) return;
    Router.replaceContent(Router.bodyContent, Router.template("admin"));
    document.getElementById("logout-button").addEventListener("click", () => Session.logout());

    let accountButton = document.getElementById("account-button");
    accountButton.textContent = Session.account.firstName + " " + Session.account.lastName;

    document.getElementById("add-station-button").addEventListener("click", function(event)
    {
        openStationEditPopup({ id: null, name: "", ownerId: null });
    });

    refreshAccountList();
    refreshStationList();
});

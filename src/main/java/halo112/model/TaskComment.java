package halo112.model;

import java.util.List;


import halo112.Database;
import halo112.data.TaskCommentDTO;

public class TaskComment {

	private int id;
	private int taskId;
	private String text;

	public static TaskComment load(int id) {
		List<TaskComment> result = Database.query("SELECT tcid, text, tid FROM Task_Comment WHERE tcid = ?", (stmt) -> {
			stmt.setInt(1, id);
		}, (row) -> {
			TaskComment comment = new TaskComment();
			comment.setId(row.getInt(1));
			comment.setText(row.getString(2));
			comment.setTaskId(row.getInt(3));
			return comment;
		});
		if (result.size() != 1)
			throw new RuntimeException("not task_comment with id " + id);
		return result.get(0);
	}

	public static List<TaskComment> loadForTask(int taskId) {
		List<TaskComment> result = Database.query("SELECT tcid, text, tid FROM Task_Comment WHERE tid = ?", (stmt) -> {
			stmt.setInt(1, taskId);
		}, (row) -> {
			TaskComment comment = new TaskComment();
			comment.setId(row.getInt(1));
			comment.setText(row.getString(2));
			comment.setTaskId(row.getInt(3));
			return comment;
		});
		return result;
	}

	public void store() {
		List<Integer> ids = Database.update("INSERT INTO Task_Comment(text, tid) VALUES (?, ?)", (stmt) -> {
			stmt.setString(1, text);
			stmt.setInt(2, taskId);
			
		}, (r) -> r.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("failed to create task_comment");
		setId(ids.get(0));
	}
	
	public static void delete(int id) { 
		Database.execute("DELETE FROM Task_Comment WHERE TCID  = ?", (stmt) -> {
			stmt.setInt(1, id);
		});
	}
	
	public TaskComment() {
	}

	public TaskComment(TaskCommentDTO taskCommentDTO) {
		this.id = taskCommentDTO.getId();
		this.taskId = taskCommentDTO.getTaskId();
		this.text = taskCommentDTO.getText();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

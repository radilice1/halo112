package halo112.model;

import java.util.List;

import halo112.Database;

public class Account {

	public static Account load(int id) {
		List<Account> result = Database.query("SELECT id, username, password, email, first_name, last_name, phone_number, ENCODE(profile_picture, 'base64'), approved, account_type_id FROM account WHERE id = ?", (stmt) -> {
			stmt.setInt(1, id);
		}, (row) -> {
			Account account = new Account();
			account.setId(row.getInt(1));
			account.setUsername(row.getString(2));
			account.setPassword(row.getString(3));
			account.setEmail(row.getString(4));
			account.setFirstName(row.getString(5));
			account.setLastName(row.getString(6));
			account.setPhone(row.getString(7));
			account.setProfilePicture(row.getString(8));
			account.setApproved(row.getBoolean(9));
			account.setAccountType(AccountType.values()[row.getInt(10)]);
			return account;
		});
		if (result.size() != 1)
			throw new RuntimeException("not account with id " + id);
		return result.get(0);
	}

	public static Account load(String username, String password) {
		List<Account> result = Database.query("SELECT id, username, password, email, first_name, last_name, phone_number, ENCODE(profile_picture, 'base64'), approved, account_type_id FROM account WHERE username = ? AND password = ?", (stmt) -> {
			stmt.setString(1, username);
			stmt.setString(2, password);
		}, (row) -> {
			Account account = new Account();
			account.setId(row.getInt(1));
			account.setUsername(row.getString(2));
			account.setPassword(row.getString(3));
			account.setEmail(row.getString(4));
			account.setFirstName(row.getString(5));
			account.setLastName(row.getString(6));
			account.setPhone(row.getString(7));
			account.setProfilePicture(row.getString(8));
			account.setApproved(row.getBoolean(9));
			account.setAccountType(AccountType.values()[row.getInt(10)]);
			return account;
		});
		if (result.size() != 1)
			throw new RuntimeException("unauthorized");
		return result.get(0);
	}
	
	public static List<Account> loadAll() {
		List<Account> result = Database.query("SELECT id, username, password, email, first_name, last_name, phone_number, ENCODE(profile_picture, 'base64'), approved, account_type_id FROM account ORDER BY id",
			null, (row) -> {
			Account account = new Account();
			account.setId(row.getInt(1));
			account.setUsername(row.getString(2));
			account.setPassword(row.getString(3));
			account.setEmail(row.getString(4));
			account.setFirstName(row.getString(5));
			account.setLastName(row.getString(6));
			account.setPhone(row.getString(7));
			account.setProfilePicture(row.getString(8));
			account.setApproved(row.getBoolean(9));
			account.setAccountType(AccountType.values()[row.getInt(10)]);
			return account;
		});
		return result;
	}

	public static void delete(int id) {
		Database.execute("DELETE FROM account WHERE id = ?", (stmt) -> {
			stmt.setInt(1, id);
		});
	}
	
	public void create() {
		List<Integer> ids = Database.update("INSERT INTO account(username, password, email, first_name, last_name, phone_number, profile_picture, approved, account_type_id) VALUES (?, ?, ?, ?, ?, ?, DECODE(?, 'base64'), ?, ?)", (stmt) -> {
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setString(3, email);
			stmt.setString(4, firstName);
			stmt.setString(5, lastName);
			stmt.setString(6, phone);
			stmt.setString(7, profilePicture);
			stmt.setBoolean(8, approved);
			stmt.setInt(9, accountType.ordinal());
		}, (r) -> r.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("failed to create user");
		setId(ids.get(0));
	}

	public void update() {
		Database.execute("UPDATE account SET username = ?, password = ?, email = ?, first_name = ?, last_name = ?, phone_number = ?, "
					   + "profile_picture = DECODE(?, 'base64'), approved = ?, account_type_id = ? WHERE id = ?", (stmt) -> {
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setString(3, email);
			stmt.setString(4, firstName);
			stmt.setString(5, lastName);
			stmt.setString(6, phone);
			stmt.setString(7, profilePicture);
			stmt.setBoolean(8, approved);
			stmt.setInt(9, accountType.ordinal());
			stmt.setInt(10, id);
		});
	}

	private int id;
	private boolean approved;
	private AccountType accountType;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String profilePicture;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public boolean getApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

}

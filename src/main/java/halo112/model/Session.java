package halo112.model;

import java.sql.Timestamp;
import java.util.List;

import halo112.Database;
import halo112.PrivilegeLevel;

public class Session {
	
	public static Session authenticate(int sessionId, PrivilegeLevel requiredLevel) {
		Session session = Session.load(sessionId);
		Account account = Account.load(session.getAccountId());
		if (!account.getApproved())
			throw new IllegalArgumentException("not registered");
		if (requiredLevel == PrivilegeLevel.ADMINISTRATOR && account.getAccountType() != AccountType.ADMINISTRATOR)
			throw new IllegalArgumentException("insufficient privileges");
		return session;
	}

	public static Session authenticateAccount(int sessionId, int accountId) {
		Session session = Session.load(sessionId);
		Account account = Account.load(session.getAccountId());
		if (accountId != account.getId() && account.getAccountType() != AccountType.ADMINISTRATOR)
			throw new IllegalArgumentException("insufficient privileges");
		return session;
	}

	public static Session load(int id) {
		Database.execute("DELETE FROM session WHERE expires < CURRENT_TIMESTAMP");

		List<Session> result = Database.query("SELECT id, expires FROM session WHERE sid = ?", (stmt) -> {
			stmt.setInt(1, id);
		}, (row) -> new Session(id, row.getInt(1), row.getTimestamp(2)));
		if (result.size() != 1)
			throw new RuntimeException("no session with this ID");
		return result.get(0);
	}

	public static int create(int accountId) {
		Database.execute("DELETE FROM session WHERE expires < CURRENT_TIMESTAMP OR id = ?",
				(s) -> s.setInt(1, accountId));

		List<Integer> key = Database.update(
				"INSERT INTO session (id, expires) VALUES (?, CURRENT_TIMESTAMP + INTERVAL '7 days')",
				(s) -> s.setInt(1, accountId), (r) -> r.getInt(1));
		if (key.size() != 1)
			throw new RuntimeException("failed to add session");
		return key.get(0);
	}

	public static void delete(int sessionId) {
		Database.execute("DELETE FROM session WHERE sid = ?", (s) -> s.setInt(1, sessionId));
	}

	private int id;
	private int accountId;
	private Timestamp expires;

	private Session(int id, int accountId, Timestamp expires) {
		this.id = id;
		this.accountId = accountId;
		this.expires = expires;
	}

	public int getId() {
		return id;
	}

	public int getAccountId() {
		return accountId;
	}

	public Timestamp getExpires() {
		return expires;
	}
}

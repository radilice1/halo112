package halo112.data;

import halo112.model.TaskComment;

public class TaskCommentDTO {

	private int id;
	private int taskId;
	private String text;
	
	public TaskCommentDTO() {
	}
	
	public TaskCommentDTO(TaskComment comment) {
		id = comment.getId();
		taskId = comment.getTaskId();
		text = comment.getText();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

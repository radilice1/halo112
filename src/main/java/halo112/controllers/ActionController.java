package halo112.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import halo112.PrivilegeLevel;
import halo112.data.ActionCalloutDTO;
import halo112.data.ActionCommentDTO;
import halo112.data.ActionDTO;
import halo112.data.ActionImageDTO;
import halo112.data.TaskCommentDTO;
import halo112.data.TaskDTO;
import halo112.model.Account;
import halo112.model.AccountType;
import halo112.model.Action;
import halo112.model.ActionCallout;
import halo112.model.ActionComment;
import halo112.model.Session;
import halo112.model.Task;
import halo112.model.TaskComment;

@RestController
public class ActionController {

	@PostMapping(value = "/action/create")
	void create(Integer sessionId, @RequestBody ActionDTO actionDTO) throws Exception {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		
		Account account = Account.load(Session.load(sessionId).getAccountId() );
		if(!account.getAccountType().equals(AccountType.DISPATCHER)) 
			throw new IllegalArgumentException("not a dispatcher");
		
		Action action = new Action(actionDTO.getId(), actionDTO.getCreatorAccountId(),
				actionDTO.getLongitude(), actionDTO.getLatitude(), actionDTO.getInfo() );
		action.store();
		
		for(TaskDTO taskDTO : actionDTO.getTasks()) {
			Task task = new Task(taskDTO);
			task.store();
			for(TaskCommentDTO taskCommentDTO : taskDTO.getComments()) {
				TaskComment taskComment = new TaskComment(taskCommentDTO);
				taskComment.store();
			}
		}
		
		for(ActionCalloutDTO calloutDTO : actionDTO.getCallouts()) {
			ActionCallout callout = new ActionCallout(calloutDTO);
			callout.store();
		}
		
		for(ActionCommentDTO actionCommentDTO : actionDTO.getComments()) {
			ActionComment actionComment = new ActionComment(actionCommentDTO);
			actionComment.store();
		}
		
		for(ActionImageDTO imageDTO : actionDTO.getImages() ) {
			Action.addImage(action.getId(), imageDTO.getImage());
		}
		
		for(int rescuer_id : actionDTO.getRescuers()) {
			Action.addParticipant(action.getId(), rescuer_id);
		}
	}
			
	@PostMapping(value = "/action/delete")
	void delete(Integer sessionId, Integer actionId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Account account = Account.load(Session.load(sessionId).getAccountId() );
		if(!account.getAccountType().equals(AccountType.DISPATCHER)) 
			throw new IllegalArgumentException("not a dispatcher");
		
		Action.delete(actionId);
		
	}

	@GetMapping(value = "/action/list")
	ActionDTO[] list(Integer sessionId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		
		return Action.list();
	}

	@PostMapping(value = "/action/addCallout")
	void addCallout(Integer sessionId, @RequestBody ActionCalloutDTO calloutDTO) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Account account = Account.load(Session.load(sessionId).getAccountId() );
		if(!account.getAccountType().equals(AccountType.DISPATCHER)) 
			throw new IllegalArgumentException("not a dispatcher");
		new ActionCallout(calloutDTO).store();
	}
	
	@PostMapping(value = "/action/respondToCallout")
	void respondToCallout(Integer sessionId, @RequestBody ActionCalloutDTO calloutDTO) {
		 Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		 Account account = Account.load(Session.load(sessionId).getAccountId() );
		 if(account.getAccountType().equals(AccountType.DISPATCHER) || account.getAccountType().equals(AccountType.ADMINISTRATOR) ) 
				throw new IllegalArgumentException("not a rescuer");
		 
		 Action.addParticipant(calloutDTO.getActionId(), account.getId());
		 ActionCallout.delete(calloutDTO);
	}

	@PostMapping(value = "/action/removeRescuer")
	void removeRescuer(Integer sessionId, Integer actionId, Integer accountId) {
		 Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		 Action.removeParticipant(accountId);
	}

	@PostMapping(value = "/action/addActionImage")
	void addActionImage(Integer sessionId, Integer actionId, @RequestBody String image) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Action.addImage(actionId, image);
	}

	@PostMapping(value = "/action/deleteActionImage")
	void deleteActionImage(Integer sessionId, Integer imageId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		Action.deleteImage(imageId);
	}

	@PostMapping(value = "/action/addActionComment")
	void addActionComment(Integer sessionId, Integer actionId, @RequestBody ActionCommentDTO comment) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		ActionComment actionComment = new ActionComment();
		actionComment.setActionId(actionId);
		actionComment.setLatitude(comment.getLatitude());
		actionComment.setLongitude(comment.getLongitude());
		actionComment.setPosterAccountId(comment.getPosterAccountId());
		actionComment.setText(comment.getText());
		actionComment.store();
	}

	@PostMapping(value = "/action/deleteActionComment")
	void deleteActionComment(Integer sessionId, Integer commentId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		ActionComment.delete(commentId);
	}

	@PostMapping(value = "/action/addTask")
	void addTask(Integer sessionId, @RequestBody TaskDTO task) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		new Task(task).store();
	}

	@PostMapping(value = "/action/addTaskComment")
	void addTaskComment(Integer sessionId, Integer taskId, @RequestBody TaskCommentDTO comment) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		TaskComment taskComment = new TaskComment();
		taskComment.setTaskId(comment.getTaskId());
		taskComment.setText(comment.getText());
		taskComment.store();
	}

	@PostMapping(value = "/action/deleteTaskComment")
	void deleteTaskComment(Integer sessionId, Integer commentId) {
		Session.authenticate(sessionId, PrivilegeLevel.REGULAR);
		TaskComment.delete(commentId);
	}

}

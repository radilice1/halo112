package halo112.data;

import halo112.model.Account;
import halo112.model.AccountType;

public class AccountDTO {

	private int id;
	private AccountType accountType;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String profilePicture;
	private boolean approved;

	public AccountDTO() {
	}

	public AccountDTO(Account acc) {
		this.id = acc.getId();
		this.accountType = acc.getAccountType();
		this.username = acc.getUsername();
		this.password = acc.getPassword();
		this.firstName = acc.getFirstName();
		this.lastName = acc.getLastName();
		this.phone = acc.getPhone();
		this.email = acc.getEmail();
		this.profilePicture = acc.getProfilePicture();
		this.approved = acc.getApproved();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

}

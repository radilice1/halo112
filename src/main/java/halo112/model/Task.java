package halo112.model;

import java.util.List;

import halo112.Database;
import halo112.data.TaskDTO;

public class Task {

	private int id;
	private int actionId;
	private int assignedAccountId;
	private String route;

	public static Task load(int id) {
		List<Task> result = Database.query("SELECT tid, route, id, aid FROM Task WHERE tid = ?", (stmt) -> {
			stmt.setInt(1, id);
		}, (row) -> {
			Task task = new Task();
			task.setId(row.getInt(1));
			task.setRoute(row.getString(2));
			task.setAssignedAccountId(row.getInt(3));
			task.setActionId(row.getInt(4));
			return task;
		});
		if (result.size() != 1)
			throw new RuntimeException("not task with id " + id);
		return result.get(0);
	}

	public static List<Task> loadForAction(int actionId) {
		List<Task> result = Database.query("SELECT tid, route, id, aid FROM Task WHERE aid = ?", (stmt) -> {
			stmt.setInt(1, actionId);
		}, (row) -> {
			Task task = new Task();
			task.setId(row.getInt(1));
			task.setRoute(row.getString(2));
			task.setAssignedAccountId(row.getInt(3));
			task.setActionId(row.getInt(4));
			return task;
		});
		return result;
	}

	public void store() {
		List<Integer> ids = Database.update("INSERT INTO Task(route, id, aid) VALUES (?, ?, ?)", (stmt) -> {
			stmt.setString(1, route);
			stmt.setInt(2, assignedAccountId);
			stmt.setInt(3, actionId);
			
		}, (r) -> r.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("failed to create task");
		setId(ids.get(0));
	}
	
	public static void delete(int id) { 
		Database.execute("DELETE FROM Task WHERE TID  = ?", (stmt) -> {
			stmt.setInt(1, id);
		});
	}
	
	public Task() {
	}
	

	public Task(TaskDTO taskDTO) {
		this.id = taskDTO.getId();
		this.actionId = taskDTO.getActionId();
		this.assignedAccountId = taskDTO.getAssignedAccountId();
		this.route = taskDTO.getRoute();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public int getAssignedAccountId() {
		return assignedAccountId;
	}

	public void setAssignedAccountId(int assignedAccountId) {
		this.assignedAccountId = assignedAccountId;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

}

package halo112.data;

import halo112.model.Action;

public class ActionDTO {

	private int id;
	private int creatorAccountId;
	private double longitude;
	private double latitude;
	private String info;
	private TaskDTO[] tasks;
	private ActionCalloutDTO[] callouts;
	private ActionCommentDTO[] comments;
	private ActionImageDTO[] images;
	private int[] rescuers;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCreatorAccountId() {
		return creatorAccountId;
	}

	public void setCreatorAccountId(int creatorAccountId) {
		this.creatorAccountId = creatorAccountId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public TaskDTO[] getTasks() {
		return tasks;
	}

	public void setTasks(TaskDTO[] tasks) {
		this.tasks = tasks;
	}

	public ActionCalloutDTO[] getCallouts() {
		return callouts;
	}

	public void setCallouts(ActionCalloutDTO[] callouts) {
		this.callouts = callouts;
	}

	public ActionCommentDTO[] getComments() {
		return comments;
	}

	public void setComments(ActionCommentDTO[] comments) {
		this.comments = comments;
	}

	public ActionImageDTO[] getImages() {
		return images;
	}

	public void setImages(ActionImageDTO[] images) {
		this.images = images;
	}

	public int[] getRescuers() {
		return rescuers;
	}

	public void setRescuers(int[] rescuers) {
		this.rescuers = rescuers;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public ActionDTO() {
	}
	
	public ActionDTO(Action action) {
		this.id = action.getId();
		this.creatorAccountId = action.getCreatorAccountId();
		this.longitude = action.getLongitude();
		this.latitude = action.getLatitude();
		this.info = action.getInfo();
		
	}
	
	

}

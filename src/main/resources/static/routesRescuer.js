import { Router } from "./router.js";
import { Session } from "./session.js";

let rescuerList = null;
let rescuerCurrent = null;
let stationList = null;
let stationCurrent = null;
let actionList = null;
let map = null;

function downsampleImage(data, maximumSize, callback) {
    let img = document.createElement("img");
    img.onload = function(event) {
        let scale = 1;
        if (img.width  * scale > maximumSize) scale = maximumSize / (img.width  * scale);
        if (img.height * scale > maximumSize) scale = maximumSize / (img.height * scale);
        let newWidth  = Math.floor(img.width  * scale);
        let newHeight = Math.floor(img.height * scale);

        let canvas = document.createElement("canvas");
        canvas.width = newWidth;
        canvas.height = newHeight;
        let context = canvas.getContext("2d");
        context.drawImage(img, 0, 0, img.width, img.height, 0, 0, newWidth, newHeight);
        callback(canvas.toDataURL("image/png"));
    }
    img.src = data;
}

function updateRescuerOnMap(rescuer) {
    if (rescuer.marker) {
        rescuer.marker.setLatLng(new L.LatLng(rescuer.latitude, rescuer.longitude));
    } else {
        let color = "gray";
        if (rescuer.id !== rescuerCurrent.id) {
            if (rescuer.accountType === "DOCTOR")      color = "green";
            if (rescuer.accountType === "FIREFIGTHER") color = "orange";
            if (rescuer.accountType === "POLICE")      color = "blue";
        }

        rescuer.marker = L.marker([rescuer.latitude, rescuer.longitude], {
            icon: L.AwesomeMarkers.icon({ icon: "user", markerColor: color })
        }).addTo(map).bindPopup(rescuer.firstName + " " + rescuer.lastName);
    }
}

let actionMarker = null;

function updateActionOnMap(action) {
    if (!action) {
        if (actionMarker)
            map.removeLayer(actionMarker);
        actionMarker = null;
        return;
    }

    let location = new L.LatLng(action.latitude, action.longitude);
    if (actionMarker) {
        actionMarker.setLatLng(location);
    } else {
        actionMarker = L.marker(location, {
            icon: L.AwesomeMarkers.icon({ icon: "exclamation", markerColor: "red" })
        }).addTo(map);
    }
}

function openStationManagementForm() {
    for (let element of document.querySelectorAll(".edit-form-container"))
        element.remove();
    Router.appendContent(Router.bodyContent, Router.template("station-management-popup"));

    let addMemberControl = document.getElementById("add-member-control");
    addMemberControl.querySelector("button").addEventListener("click", (event) => {
        let idToAdd = parseInt(addMemberControl.querySelector("select").value);
        for (let rescuer of rescuerList) {
            if (rescuer.stationId !== stationCurrent.id) {
                rescuer.stationId = stationCurrent.id;
                fetch("/station/addMember?" + new URLSearchParams({ sessionId: Session.id, stationId: stationCurrent.id, accountId: rescuer.id }), { method: "post" });
                openStationManagementForm();
                break;
            }
        }
    });

    let atLeastOneToAdd = false;
    for (let rescuer of rescuerList) {
        let accountTypeName = undefined;
        if (rescuer.accountType === "DOCTOR")        accountTypeName = "Doktor";
        if (rescuer.accountType === "FIREFIGTHER")   accountTypeName = "Vatrogasac";
        if (rescuer.accountType === "POLICE")        accountTypeName = "Policajac";

        let name = rescuer.firstName + " " + rescuer.lastName + " (" + accountTypeName + ")";

        if (rescuer.stationId === stationCurrent.id && rescuer !== rescuerCurrent) {
            let currentMembers = document.getElementById("current-members");
            Router.appendContent(currentMembers, Router.template("station-management-member-control"));
            currentMembers.lastElementChild.querySelector("span").textContent = name;
            currentMembers.lastElementChild.querySelector("button").addEventListener("click", (event) => {
                rescuer.stationId = null;
                fetch("/station/removeMember?" + new URLSearchParams({ sessionId: Session.id, stationId: stationCurrent.id, accountId: rescuer.id }), { method: "post" });
                openStationManagementForm();
            });
        } else if (rescuer.stationId === null) {
            let option = document.createElement("option");
            option.setAttribute("value", rescuer.id);
            option.textContent = name;
            addMemberControl.querySelector("select").appendChild(option);
            atLeastOneToAdd = true;
        }
    }

    if (!atLeastOneToAdd)
        addMemberControl.remove();

    document.getElementById("edit-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".edit-form-container"))
            element.remove();
    });
}

function openTaskComments(task) {
    for (let element of document.querySelectorAll(".edit-form-container"))
        element.remove();
    let templateDiv = Router.appendContent(Router.bodyContent, Router.template("task-comments"));
    templateDiv.setAttribute("data-task-comments-id", task.id);

    let comments = task.comments;
    comments.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let comment of comments) {
        let poster = "Dispečer";
        for (let rescuer of rescuerList)
            if (rescuer.id == comment.posterAccountId)
                poster = rescuer.firstName + " " + rescuer.lastName;

        let commentsDiv = document.getElementById("task-comments-div");
        let commentDiv = Router.appendContent(commentsDiv, Router.template("task-comment"))
        commentDiv.querySelector(".poster").textContent = poster;
        commentDiv.querySelector(".content").textContent = comment.text;
    }

    for (let el of templateDiv.querySelectorAll(".not-in-rescuer"))
        el.remove();

    document.getElementById("edit-cancel-button").addEventListener("click", (event) => {
        for (let element of document.querySelectorAll(".edit-form-container"))
            element.remove();
    });
}

let commentOnMapMarker = null;
let commentOnMapMarkerId = null;
let routeOnMap = null;
let routeOnMapId = null;

let trustTheActionId = false;

function showActionJoiningUI() {
    for (let panel of document.querySelectorAll(".action-panel"))
        panel.remove();

    Router.appendContent(document.querySelector(".sidebar"), Router.template("rescuer-join"));

    let qualifiedDiv   = document.querySelector(".qualified-callouts");
    let unqualifiedDiv = document.querySelector(".unqualified-callouts");

    let allCallouts = [];
    for (let action of actionList)
        for (let callout of action.callouts)
            allCallouts.push(callout);

    let urgencies = [ "LOW", "MEDIUM", "HIGH", "VERY_HIGH" ];
    allCallouts.sort((a, b) => {
        let ai = urgencies.indexOf(a.urgency);
        let bi = urgencies.indexOf(b.urgency);
        if (ai > bi) return -1;
        if (ai < bi) return  1;
        return 0;
    })

    for (let callout of allCallouts) {
        let isQualified = (callout.accountType == Session.account.accountType);

        let role = "<nepoznata uloga>";
        if (callout.accountType == "DOCTOR") role = "Doktor";
        if (callout.accountType == "FIREFIGTHER") role = "Vatrogasac";
        if (callout.accountType == "POLICE") role = "Policajac";
        let vehicle = "<nepoznato vozilo>";
        if (callout.methodOfTransportation == "MOTORCYCLE")      vehicle = "Motocikl";
        if (callout.methodOfTransportation == "AMBULANCE")       vehicle = "Hitna pomoć";
        if (callout.methodOfTransportation == "TANK_TRUCK")      vehicle = "Autocisterna";
        if (callout.methodOfTransportation == "CAR_LADDERS")     vehicle = "Autoljestve";
        if (callout.methodOfTransportation == "COMMAND_VEHICLE") vehicle = "Zapovjedno vozilo";
        if (callout.methodOfTransportation == "FOREST_VEHICLE")  vehicle = "Šumsko vozilo";
        if (callout.methodOfTransportation == "PEDESTRIAN")      vehicle = "Pješak";
        if (callout.methodOfTransportation == "CAR")             vehicle = "Automobil";
        if (callout.methodOfTransportation == "ARMORED_VEHICLE") vehicle = "Oklopno vozilo";
        let urgency = "<nepoznata hitnost>";
        if (callout.urgency == "LOW") urgency = "Niska";
        if (callout.urgency == "MEDIUM") urgency = "Srednja";
        if (callout.urgency == "HIGH") urgency = "Visoka";
        if (callout.urgency == "VERY_HIGH") urgency = "Vrlo visoka";

        let description = role + ", " + vehicle + ", " + urgency + " hitnost";

        if (isQualified) {
            let p = Router.appendContent(qualifiedDiv, Router.template("rescuer-callout"))
            p.querySelector(".content").textContent = description;
            p.querySelector(".joinButton").addEventListener("click", () => {
                setCurrentAction(callout.actionId);
                trustTheActionId = true;
                fetch("/action/respondToCallout?" + new URLSearchParams({ sessionId: Session.id }), {
                    method: "post",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify(callout)
                });
            });
        } else {
            let p = document.createElement("p");
            p.textContent = description;
            unqualifiedDiv.appendChild(p);
        }
    }
}

function clearActionRelatedMarkers() {
    if (commentOnMapMarker)
        map.removeLayer(commentOnMapMarker);
    if (routeOnMap)
        map.removeLayer(routeOnMap);
    if (actionMarker)
        map.removeLayer(actionMarker);
    commentOnMapMarker = null;
    commentOnMapMarkerId = null;
    routeOnMap = null;
    routeOnMapId = null;
    actionMarker = null;

    for (let rescuer of rescuerList) {
        if (rescuer.id == rescuerCurrent.id || !rescuer.marker) continue;
        map.removeLayer(rescuer.marker);
        rescuer.marker = null;
    }
}

function setCurrentAction(actionId) {
    rescuerCurrent.actionId = actionId;
    let action = actionList.find(x => x.id == actionId);
    updateActionOnMap(action);

    for (let panel of document.querySelectorAll(".action-panel"))
        panel.remove();
    if (!action) {
        clearActionRelatedMarkers();
        showActionJoiningUI();
        return;
    }

    Router.appendContent(document.querySelector(".sidebar"), Router.template("action"));
    document.getElementById("action-info").textContent = action.info;
    document.querySelector(".action-title-h2").textContent = "Trenutačna akcija:";

    for (let el of document.querySelectorAll(".not-in-rescuer"))
        el.remove();

    let images = action.images;
    images.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let image of images) {
        let img = document.createElement("img");
        img.src = atob(image.image);
        document.getElementById("action-images-div").appendChild(img);
    }

    function addComment(comment) {
        let poster = "Dispečer";
        for (let rescuer of rescuerList)
            if (rescuer.id == comment.posterAccountId)
                poster = rescuer.firstName + " " + rescuer.lastName;

        let commentsDiv = document.getElementById("action-comments-div");
        let commentDiv = Router.appendContent(commentsDiv, Router.template("action-comment"))
        commentDiv.querySelector(".poster").textContent = poster;
        commentDiv.querySelector(".content").textContent = comment.text;

        commentDiv.querySelector(".showOnMapButton").addEventListener("click", () => {
            if (commentOnMapMarker) {
                map.removeLayer(commentOnMapMarker);
                commentOnMapMarker = null;
            }

            if (commentOnMapMarkerId == comment.id) {
                commentOnMapMarkerId = null;
                return;
            }

            commentOnMapMarkerId = comment.id;
            commentOnMapMarker = L.marker([ comment.latitude, comment.longitude ], {
                icon: L.AwesomeMarkers.icon({ icon: "comment", markerColor: "gray" })
            }).addTo(map).bindPopup(comment.text).openPopup();
        });

        for (let el of commentDiv.querySelectorAll(".not-in-rescuer"))
            el.remove();
    }

    let comments = action.comments;
    comments.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let comment of comments)
        addComment(comment);


    function showTaskRoute(task) {
        if (routeOnMap) {
            map.removeLayer(routeOnMap);
            routeOnMap = null;
        }

        if (routeOnMapId == task.id) {
            routeOnMapId = null;
            return;
        }

        routeOnMapId = task.id;
        routeOnMap = new L.Polyline.fromEncoded(task.route, {
            color: "red",
            weight: 3,
            opacity: 0.5,
            smoothFactor: 1
        });
        routeOnMap.addTo(map);
    }

    let tasks = action.tasks.filter(x => x.assignedAccountId == rescuerCurrent.id);
    tasks.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return  1;
        return 0;
    });
    for (let task of tasks)
        if (document.querySelectorAll("[data-task-comments-id='" + task.id + "']").length)
            openTaskComments(task);
    for (let rescuerId of action.rescuers) {
        let rescuer = rescuerList.find(x => x.id == rescuerId);
        if (!rescuer) continue;

        let rescuersDiv = document.getElementById("action-rescuers-div");
        let rescuerDiv = Router.appendContent(rescuersDiv, Router.template("action-rescuer"))
        rescuerDiv.querySelector(".name").textContent = rescuer.firstName + " " + rescuer.lastName;

        for (let rescuerDiv of document.querySelectorAll(".not-in-rescuer"))
            rescuerDiv.remove();

        let tasksDiv = rescuerDiv.querySelector(".rescuer-tasks");
        for (let task of tasks.filter(x => x.assignedAccountId == rescuer.id)) {
            let taskDiv = Router.appendContent(tasksDiv, Router.template("action-rescuer-task"))
            taskDiv.querySelector(".taskIndex").textContent = tasksDiv.querySelectorAll(".action-rescuer-task").length;
            taskDiv.querySelector(".showOnMapButton").addEventListener("click", () => showTaskRoute(task));
            taskDiv.querySelector(".commentButton").addEventListener("click", () => {
                openTaskComments(task);
            });
        }
    }


    document.querySelector(".action-image-upload").addEventListener("click", (event) => {
        document.querySelector(".action-image-file").click();
    });
    document.querySelector(".action-image-file").addEventListener("change", (event) => {
        for (let file of event.srcElement.files) {
            let reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = function(event) {
                let dataUrl = `data:${file.type};base64,${btoa(event.target.result)}`;
                downsampleImage(dataUrl, 300, (downsampledDataUrl) => {
                    let img = document.createElement("img");
                    img.setAttribute("src", downsampledDataUrl);
                    document.getElementById("action-images-div").appendChild(img);

                    fetch("/action/addActionImage?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id }), {
                        method: "post",
                        body: btoa(downsampledDataUrl)
                    });
                });
            };
            reader.onerror = function() {
                console.error(reader.error);
            };
        }
    });

    document.getElementById("action-comment-form").addEventListener("submit", (event) => {
        event.preventDefault();

        let comment = {
            id: 0,
            actionId: action.id,
            posterAccountId: Session.account.id,
            text: document.getElementById("comment-text").value,
            latitude:  rescuerCurrent.latitude  || action.latitude,
            longitude: rescuerCurrent.longitude || action.longitude
        };

        fetch("/action/addActionComment?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id }), {
            method: "post",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(comment)
        });

        addComment(comment);
        document.getElementById("comment-text").value = "";
        return false;
    });

    document.getElementById("close-action-button").addEventListener("click", (event) => {
        fetch("/action/removeRescuer?" + new URLSearchParams({ sessionId: Session.id, actionId: action.id, accountId: rescuerCurrent.id }), { method: "post" });
        for (let panel of document.querySelectorAll(".action-panel"))
            panel.remove();
        clearActionRelatedMarkers();
        showActionJoiningUI();
    });
}

Router.register("#rescuer", (query) => {
    if (!Session.checkLoggedIn()) return;
    L.AwesomeMarkers.Icon.prototype.options.prefix = "fa";

    let rescuerFetch = fetch("/rescuer/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" });
    let stationFetch = fetch("/station/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" });
    let actionFetch  = fetch("/action/list?"  + new URLSearchParams({ sessionId: Session.id }), { method: "get" });
    Promise.all([ rescuerFetch, stationFetch, actionFetch ])
    .then(async (responses) => {
        if (responses[0].status == 200 && responses[1].status == 200 && responses[2].status == 200) {
            rescuerList = await responses[0].json();
            stationList = await responses[1].json();
            actionList  = await responses[2].json();

            rescuerCurrent = null;
            for (let rescuer of rescuerList)
                if (rescuer.id === Session.account.id)
                    rescuerCurrent = rescuer;
            if (!rescuerCurrent)
                throw new Error("can't find current rescuer");

            stationCurrent = null;
            if (rescuerCurrent.stationId !== null)
                for (let station of stationList)
                    if (station.id === rescuerCurrent.stationId)
                        stationCurrent = station;

            if (!stationCurrent) {
                Router.replaceContent(Router.bodyContent, Router.template("rescuer-unassigned"));
                document.getElementById("logout-button").addEventListener("click", () => Session.logout());
                return;
            }

            Router.replaceContent(Router.bodyContent, Router.template("rescuer"));
            document.getElementById("logout-button").addEventListener("click", () => Session.logout());

            let accountButton = document.getElementById("account-button");
            accountButton.textContent = Session.account.firstName + " " + Session.account.lastName;

            let isStationOwner = rescuerCurrent.id === stationCurrent.ownerId;
            let stationButton = document.getElementById("station-button");
            stationButton.textContent = stationCurrent.name;
            stationButton.addEventListener("click", function(event) {
                if (isStationOwner) {
                    openStationManagementForm();
                }
            });

            document.getElementById("rescuer-available").checked = rescuerCurrent.available;
            document.getElementById("rescuer-available").addEventListener("change", () => {
                rescuerCurrent.available = !rescuerCurrent.available;
                fetch("/rescuer/updateAvailable?" + new URLSearchParams({
                    sessionId: Session.id,
                    available: rescuerCurrent.available
                }), { method: "post" });
            });


            map = L.map("map").setView([45.815399, 15.966568], 13);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

            let bounds = L.latLngBounds(L.latLng(45.815399 - 0.5, 15.966568 - 0.5),
                                        L.latLng(45.815399 + 0.5, 15.966568 + 0.5));
            map.setMaxBounds(bounds);
            map.on('drag', () => {
                map.panInsideBounds(bounds, { animate: false });
            });


            setCurrentAction(rescuerCurrent.actionId);


            let artificialOffset = [0, 0];
            let pingLocation = () => {
                fetch("/rescuer/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" })
                .then(async (response) => {
                    setTimeout(pingLocation, 2000);
                    if (response.status != 200) return;

                    let cookie = Symbol();
                    for (let updatedRescuer of await response.json()) {
                        let rescuer = null;
                        for (let existingRescuer of rescuerList) {
                            if (existingRescuer.id == updatedRescuer.id) {
                                rescuer = existingRescuer;
                                break;
                            }
                        }
                        if (!rescuer) {
                            rescuerList.push(updatedRescuer);
                            rescuer = updatedRescuer;
                        }
                        rescuer.cookie = cookie;

                        if (updatedRescuer.id === rescuerCurrent.id) {
                            if (trustTheActionId)
                                trustTheActionId = false;
                            else if (rescuerCurrent.actionId !== updatedRescuer.actionId)
                                setCurrentAction(updatedRescuer.actionId);
                            else if (!rescuerCurrent.actionId)
                                showActionJoiningUI();
                        } else {
                            rescuer.longitude = updatedRescuer.longitude;
                            rescuer.latitude  = updatedRescuer.latitude;
                            rescuer.available = updatedRescuer.available;
                            if (rescuer.available && rescuer.latitude && rescuer.longitude &&
                                rescuer.actionId && rescuer.actionId == rescuerCurrent.actionId) {
                                updateRescuerOnMap(rescuer);
                            } else if (rescuer.marker) {
                                map.removeLayer(rescuer.marker);
                                rescuer.marker = null;
                            }
                        }
                    }
                    for (let removedRescuer of rescuerList.filter(x => x.cookie != cookie))
                        if (removedRescuer.marker)
                            map.removeLayer(removedRescuer.marker);
                    rescuerList = rescuerList.filter(x => x.cookie == cookie);
                });

                navigator.geolocation.getCurrentPosition((position) => {
                    rescuerCurrent.latitude  = position.coords.latitude  + artificialOffset[0];
                    rescuerCurrent.longitude = position.coords.longitude + artificialOffset[1];
                    updateRescuerOnMap(rescuerCurrent);

                    fetch("/rescuer/updateLocation?" + new URLSearchParams({
                        sessionId: Session.id,
                        latitude: rescuerCurrent.latitude,
                        longitude: rescuerCurrent.longitude
                    }), { method: "post" });

                    artificialOffset[0] += 0.002;
                    artificialOffset[1] += 0.0002;
                });
            };
            pingLocation();


            async function updateActions() {
                let actionFetch = await fetch("/action/list?" + new URLSearchParams({ sessionId: Session.id }), { method: "get" });
                setTimeout(updateActions, 2000);
                let oldCurrentAction = actionList.find(x => x.id = rescuerCurrent.actionId);
                actionList = await actionFetch.json();
                let newCurrentAction = actionList.find(x => x.id = rescuerCurrent.actionId);

                if (JSON.stringify(oldCurrentAction) !== JSON.stringify(newCurrentAction)) {
                    setCurrentAction(rescuerCurrent.actionId);
                }
                else if (!rescuerCurrent.actionId)
                    showActionJoiningUI();
            }
            updateActions();
        } else {
            Session.removeFromStorage();
        }
    })
    .catch((error) => {
        console.error(error);
        Session.removeFromStorage();
    });
});


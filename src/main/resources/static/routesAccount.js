import { Router } from "./router.js";
import { Session } from "./session.js";

Router.register("#login", (query) => {
    Router.replaceContent(Router.bodyContent, Router.template("login"));

    document.querySelector(".error").textContent = query.get("error") || "";

    document.getElementById("login-form").addEventListener("submit", (event) => {
        event.preventDefault();
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;

        let errorParams = "?" + new URLSearchParams({ error: "Ne postoji račun s unesenim korisničkim imenom ili lozinkom." });
        Session.login("#", errorParams + "#login", username, password);

        return false;
    });
});

Router.register("#register", (query) => {
    Router.replaceContent(Router.bodyContent, Router.template("register"));

    document.querySelector(".error").textContent = query.get("error") || "";

    let state = {
        profilePicture: ""
    };

    let profilePictureButton = document.getElementById("profile-picture-button");
    let profilePictureInput  = document.getElementById("profile-picture");

    profilePictureButton.addEventListener("click", (event) => {
        profilePictureInput.click();
    });

    profilePictureInput.addEventListener("change", (event) => {
        for (let file of event.srcElement.files) {
            let reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = function(event) {
                state.profilePicture = `data:${file.type};base64,${btoa(event.target.result)}`;
                profilePictureButton.style.backgroundImage = `url("${state.profilePicture}")`;
            };
            reader.onerror = function() {
                console.error(reader.error);
            };
        }
    });

    document.getElementById("registration-form").addEventListener("submit", (event) => {
        event.preventDefault();
        try {
            let errorParams = "?" + new URLSearchParams({ error: "Došlo je do greške. Neki od unesenih podataka već postoje u bazi." });
            Session.register("#registration-submitted", errorParams + "#register", {
                id:             0,
                accountType:    document.getElementById("role").value,
                username:       document.getElementById("username").value,
                password:       document.getElementById("password").value,
                firstName:      document.getElementById("first-name").value,
                lastName:       document.getElementById("last-name").value,
                phone:          document.getElementById("phone").value,
                email:          document.getElementById("email").value,
                profilePicture: btoa(state.profilePicture),
                approved:       false,
            });
        } catch(error) {
            console.error(error);
        }
        return false;
    });
});

Router.register("#registration-submitted", (query) => {
   Router.replaceContent(Router.bodyContent, Router.template("registration-submitted"));
})

package halo112;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class SeleniumStationManagerTest {

    @Test
    public void stationManager() throws InterruptedException {

        // SETUP
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.get("https://fer-progi2021-radilice-halo112.herokuapp.com");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("jebomajku");

        // LOGIN

        driver.findElement(By.id("username")).sendKeys("kukuruz");
        driver.findElement(By.id("password")).sendKeys("kukuruz");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        TimeUnit.SECONDS.sleep(2);

        //POZICIONIRAJ SE U STANICU

        driver.findElement(By.id("station-button")).click();
        TimeUnit.MILLISECONDS.sleep(200);

        // DODAJ SPASIOCA
        driver.findElement(By.id("add-member-control")).findElement(By.cssSelector("button")).click();
        TimeUnit.MILLISECONDS.sleep(500);

        // OBRISI SPASIOCA
        driver.findElement(By.id("current-members")).findElement(By.className("station-member-control")).findElement(By.cssSelector("button")).click();
        assertTrue(true);

        driver.close();
        driver.quit();
    }

}

package halo112;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDispatcherTest {
	
	private WebDriver driver;
		
	@BeforeEach
	private void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Drives\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test
	public void testLoginDispatcherGoodCreds() {
		driver.get("https://fer-progi2021-radilice-halo112.herokuapp.com/");
		WebElement element = driver.findElement(By.name("username"));
		element.sendKeys("mrkva");
		element = driver.findElement(By.name("password"));
		element.sendKeys("mrkva");
		driver.findElement(By.cssSelector("button[type='submit']")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String redirURL = driver.getCurrentUrl();
		boolean result = redirURL.contains("dispatcher");
		assertTrue(result);
	}
	
	@Test
	public void testLoginDispatcherInvalidCreds() {
		driver.get("https://fer-progi2021-radilice-halo112.herokuapp.com/");
		WebElement element = driver.findElement(By.name("username"));
		element.sendKeys("abc");
		element = driver.findElement(By.name("password"));
		element.sendKeys("123");
		driver.findElement(By.cssSelector("button[type='submit']")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String redirURL = driver.getCurrentUrl();
		boolean result = redirURL.contains("dispatcher");
		assertFalse(result);
	}
	
	@AfterEach
	private void end() {
		driver.quit();
	}



}

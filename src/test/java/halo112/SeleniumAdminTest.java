package halo112;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumAdminTest {

    @Test
    public void TestLoginGoodCreds() throws InterruptedException {

        // ULOGIRAJ SE KAO ADMIN I NAPRAVI NOVU STANICU, ZATIM JU IZBRISI

        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("https://fer-progi2021-radilice-halo112.herokuapp.com/#login");

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("admin");

        WebElement password = driver.findElement(By.name("password"));
        password.sendKeys("admin");

        driver.findElement(By.cssSelector("button[type='submit']")).click();
        TimeUnit.SECONDS.sleep(2);

        WebElement dodajStanicu = driver.findElement(By.id("add-station-button"));
        dodajStanicu.click();

        WebElement ime = driver.findElement(By.id("name"));
        ime.sendKeys("Rebro");

        Select owner = new Select(driver.findElement(By.id("owner")));
        owner.selectByValue("18");      // Ananas je voditelj

        driver.findElement(By.cssSelector("button[type='submit']")).click();
        TimeUnit.SECONDS.sleep(2);

        WebElement form = driver.findElement(By.id("confirm-form"));
        form.findElement(By.cssSelector("button[type='submit'")).click();
        TimeUnit.SECONDS.sleep(2);

        List<WebElement> stanice = driver.findElement(By.id("station-list")).findElements(By.className("station"));
        WebElement result;
        boolean uspjeh = false;

        for (int i = 0; i < stanice.size(); i++) {
            if(stanice.get(i).findElement(By.className("lhs")).findElement(By.className("station-short-info")).getText().equals("Rebro")) {
                result = stanice.get(i);
                WebElement delete = result.findElement(By.className("rhs")).findElement(By.className("delete"));
                delete.click();
                driver.findElement(By.id("confirm-form")).click();
                uspjeh = true;

            }
        }

        assertTrue(uspjeh);
        driver.close();
        driver.quit();

    }


}

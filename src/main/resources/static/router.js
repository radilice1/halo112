export let Router = {
    routes: new Map(),

    register: function(route, handler) {
        this.routes.set(route, handler);
    },

    apply: function() {
        let hash  = window.location.hash;
        let route = this.routes.get(hash);
        if (route === undefined)
            route = this.routes.get(null);
        route(new URLSearchParams(window.location.search));
    },

    follow: function(url) {
        history.pushState(null, "", window.location.origin + url);
        this.apply();
    },

    bodyContent: document.getElementById("body-content"),
    template: function(name) {
        return document.getElementById("template-" + name);
    },

    replaceContent: function(where, template) {
        let clone = template.content.cloneNode(true);
        while (where.firstChild)
            where.removeChild(where.lastChild);
        where.appendChild(clone);
        for (let link of where.querySelectorAll("button[data-href]"))
            link.addEventListener("click", () => this.follow(link.getAttribute("data-href")));
        Router.expandTemplates(where);
    },

    appendContent: function(where, template) {
        let clone = template.content.cloneNode(true);
        where.appendChild(clone);
        let appended = where.lastElementChild;
        for (let link of appended.querySelectorAll("button[data-href]"))
            link.addEventListener("click", () => this.follow(link.getAttribute("data-href")));
        Router.expandTemplates(where);
        return appended;
    },

    expandTemplates: function(where) {
        for (let container of where.querySelectorAll("[data-template]")) {
            let template = Router.template(container.getAttribute("data-template"));
            Router.replaceContent(container, template);
        }
    }
}

window.addEventListener("popstate", () => Router.apply());

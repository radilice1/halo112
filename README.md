# Halo112

Link na Heroku app: [fer-progi2021-radilice-halo112.herokuapp.com](https://fer-progi2021-radilice-halo112.herokuapp.com/)

Budući da je ovo free-tier Heroku app, prvi put kada se stranica učita nakon nekog vremena treba 10ak sekundi da se upali Heroku-ov VM.

Registrirani accountovi su:

| Korisničko ime | lozinka   | Vrsta         |
|----------------|-----------|---------------|
| admin          | admin     | Administrator |
| test           | test      | Doktor        |
| test2          | test2     | Vatrogasac    |
| test3          | test3     | Dispečer      |
| lubenica       | lubenica  | Policajac     |
| ananas         | ananas    | Policajac     |
| mrkva          | mrkva     | Dispečer      |
| kukuruz        | kukuruz   | Vatrogasac    |
| krastavac      | krastavac | Doktor        |

Dokumentacija se nalazi u ovom repozitoriju: [PROGI_2021_Radilice_v2_0.pdf](https://gitlab.com/radilice1/halo112/-/blob/main/PROGI_2021_Radilice_v2_0.pdf)

Prezentacija projekta se također nalazi u ovom repozitoriju: [PrezentacijaProjekta.pptx](https://gitlab.com/radilice1/halo112/-/blob/main/PrezentacijaProjekata.pptx), [PrezentacijaProjekta.pdf](https://gitlab.com/radilice1/halo112/-/blob/main/PrezentacijaProjekata.pdf)

package halo112.model;

import java.util.List;

import halo112.Database;
import halo112.data.ActionCalloutDTO;
import halo112.data.ActionCommentDTO;
import halo112.data.ActionDTO;
import halo112.data.ActionImageDTO;
import halo112.data.TaskCommentDTO;
import halo112.data.TaskDTO;

public class Action {

	public static Action load(int id) {
		List<Action> result =
				Database.query("SELECT aid, dispatcher_id, longitude, latitude, info FROM action WHERE id = ?", (stmt) -> {
			stmt.setInt(1, id);
		}, (row) -> {
			Action action = new Action();
			action.setId(row.getInt(1));
			action.setCreatorAccountId(row.getInt(2));
			action.setLongitude(row.getDouble(3));
			action.setLatitude(row.getDouble(4));
			action.setInfo(row.getString(5));
			return action;
		});
		if (result.size() != 1)
			throw new RuntimeException("not action with id " + id);
		return result.get(0);
	}
	
	public static List<Action> loadAll() {
		List<Action> result =
				Database.query("SELECT aid, dispatcher_id, longitude, latitude, info FROM action",
					null, (row) -> {
			Action action = new Action();
			action.setId(row.getInt(1));
			action.setCreatorAccountId(row.getInt(2));
			action.setLongitude(row.getDouble(3));
			action.setLatitude(row.getDouble(4));
			action.setInfo(row.getString(5));
			return action;
		});
		return result;
	}

	public static int addImage(int id, String image) {
		List<Integer> ids = Database.update("INSERT INTO Action_Image(image, aid) VALUES (DECODE(?, 'base64'), ?)", (stmt) -> {
			stmt.setString(1, image);
			stmt.setInt(2, id);
	
		}, (r) -> r.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("failed to create action_image");
		return ids.get(0);
	}
	
	public static void deleteImage(int id) { 
		Database.execute("DELETE FROM Action_Image WHERE iid = ?", (stmt) -> {
			stmt.setInt(1, id);
		});
	}

	public static void addParticipant(int id, int participantId) {
		Database.execute("UPDATE Rescuer SET aid = ? WHERE id = ?", (stmt) -> {
			stmt.setInt(1, id);
			stmt.setInt(2, participantId);
		});
	}
	
	public static void removeParticipant( int participantId) {
		Database.execute("UPDATE Rescuer SET AID = null WHERE ID = ?", (stmt) -> {
			stmt.setInt(1, participantId);
		});
	}

	public void store() {
		List<Integer> ids = Database.update("INSERT INTO Action"
				+ "(dispatcher_id, longitude, latitude, info) "
				+ "VALUES (?, ?, ?, ?)", (stmt) -> {
			stmt.setInt(1, creatorAccountId);
			stmt.setDouble(2, longitude);
			stmt.setDouble(3, latitude);
			stmt.setString(4, info);
		}, (r) -> r.getInt(1));
		if (ids.size() != 1)
			throw new RuntimeException("failed to create action");
		setId(ids.get(0));
	}
	
	public static void delete(Integer actionId) {
		Database.execute("DELETE FROM Task_Comment WHERE tid IN (SELECT tid FROM task WHERE aid = ?)", (stmt) -> stmt.setInt(1, actionId));
		Database.execute("DELETE FROM Task WHERE aid = ?", (stmt) -> stmt.setInt(1, actionId));
		Database.execute("DELETE FROM Action_Callout WHERE aid = ?", (stmt) -> stmt.setInt(1, actionId));
		Database.execute("DELETE FROM Action_Comment WHERE aid = ?", (stmt) -> stmt.setInt(1, actionId));
		Database.execute("DELETE FROM Action_Image WHERE aid = ?", (stmt) -> stmt.setInt(1, actionId));
		Database.execute("UPDATE Rescuer SET aid = null WHERE aid = ?", (stmt) -> stmt.setInt(1, actionId));
		Database.execute("DELETE FROM Action WHERE aid = ?", (stmt) -> stmt.setInt(1, actionId));
	}
	

	private int id;
	private int creatorAccountId;
	private double longitude;
	private double latitude;
	private String info;
	private String[] images;
	private List<Integer> participants;
	
	
	public Action() {};
	
	public Action(int id, int creatorAccountId, double longitude, double latitude, String info) {
		this.id = id;
		this.creatorAccountId = creatorAccountId;
		this.longitude = longitude;
		this.latitude = latitude;
		this.info = info;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCreatorAccountId() {
		return creatorAccountId;
	}

	public void setCreatorAccountId(int creatorAccountId) {
		this.creatorAccountId = creatorAccountId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String[] getImages() {
		return images;
	}

	public List<Integer> getParticipants() {
		return participants;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	
	public static List<ActionImageDTO> loadImagesForAction(Integer actionId){
		return Database.query("SELECT iid, ENCODE(image, 'base64'), aid FROM Action_Image WHERE aid = ?", (stmt) -> {
			stmt.setInt(1, actionId);
		}, (row) -> {
			ActionImageDTO image = new ActionImageDTO();
			image.setId(row.getInt(1));
			image.setImage(row.getString(2));
			return image;
		});
	}
	
	public static int[] loadRescuersForAction(Integer actionId){
		List<Integer> result =
				Database.query("SELECT id from Rescuer WHERE aid = ?", (stmt) -> {
						stmt.setInt(1, actionId);
					}, (row) -> {
			return row.getInt(1);
		});
		int[] array = new int[result.size()];
		for(int i = 0; i < result.size(); i++) 
			array[i] = result.get(i);
		return array;
	}
	
	public static ActionDTO[] list() {
	
		List<Action> actions = loadAll();
		int k = 0;
		ActionDTO[] list = new ActionDTO[actions.size()];
		for(Action action : actions) {
			
			ActionDTO actionDTO = new ActionDTO(action);
			List<Task> tasks = Task.loadForAction(action.getId()) ;
			TaskDTO[] tasksDTO = new TaskDTO[tasks.size()];
			for(int i = 0; i < tasks.size(); i++) {
				TaskDTO taskDTO = new TaskDTO(tasks.get(i));
				taskDTO.setComments(TaskComment.loadForTask(tasks.get(i).getId()).stream().map((x) -> new TaskCommentDTO(x)).toList().toArray(new TaskCommentDTO[0]));
				tasksDTO[i] = taskDTO;
			}
			
			actionDTO.setTasks(tasksDTO);
			actionDTO.setCallouts(ActionCallout.loadForAction(action.getId()).stream().map((x) -> new ActionCalloutDTO(x)).toList().toArray(new ActionCalloutDTO[0]));
			actionDTO.setComments(ActionComment.loadForAction(action.getId()).stream().map((x) -> new ActionCommentDTO(x)).toList().toArray(new ActionCommentDTO[0]));
			actionDTO.setImages(loadImagesForAction(action.getId()).toArray(new ActionImageDTO[0]));
			actionDTO.setRescuers(loadRescuersForAction(action.getId()));
			 
			list[k++] = actionDTO;
		
		}
		return list;
	}

}

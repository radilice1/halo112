package halo112.data;

import halo112.model.ActionComment;

public class ActionCommentDTO {

	private int id;
	private int actionId;
	private int posterAccountId;
	private double longitude;
	private double latitude;
	private String text;
	
	public ActionCommentDTO() {
	}
	
	public ActionCommentDTO(ActionComment comment) {
		id = comment.getId();
		actionId = comment.getActionId();
		posterAccountId = comment.getPosterAccountId();
		longitude = comment.getLongitude();
		latitude = comment.getLatitude();
		text = comment.getText();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public int getPosterAccountId() {
		return posterAccountId;
	}

	public void setPosterAccountId(int posterAccountId) {
		this.posterAccountId = posterAccountId;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
